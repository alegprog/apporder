<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description',255);
            $table->string('barcode');
            $table->boolean('available');
            $table->integer('order');
            $table->string('preparation');
            $table->double('cost',15, 2);
            $table->double('price',15, 2);
            $table->double('moneymaking',15, 2);
            $table->string('image');
            $table->integer('classification_id')->unsigned();
            $table->foreign('classification_id')->references('id')->on('classifications');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->integer('subgroup_id')->unsigned();
            $table->foreign('subgroup_id')->references('id')->on('subgroups');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
