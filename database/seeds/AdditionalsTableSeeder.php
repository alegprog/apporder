<?php

use Illuminate\Database\Seeder;
use App\Entities\Additional;

class AdditionalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $additionals=[
        ['name'=>'BORDA DE CATUPIRY','price'=>'6.00','available'=>0],
        ['name'=>'ADICIONAL DE BACON','price'=>'3.00','available'=>0],
        ['name'=>'BORDA DE CHEDAR','price'=>'6.00','available'=>0],
        ['name'=>'ADICIONAL DE MUSSARELA','price'=>'6.00','available'=>0],
        ['name'=>'LIMÃO ESPREMIDO','price'=>'1.00','available'=>0],
      ];

      foreach($additionals as $additional){
        $new= new Additional();
        $new->name=$additional['name'];
        $new->price=$additional['price'];
        $new->available=$additional['available'];
        $new->save();
      }

    }
}
