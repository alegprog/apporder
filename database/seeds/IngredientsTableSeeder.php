<?php

use Illuminate\Database\Seeder;
use App\Entities\Ingredient;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $ingredients=[
        ['name'=>'FARINHA DE TRIGO','available'=>0],
        ['name'=>'MOLHO DE TOMATE','available'=>1],
        ['name'=>'CEBOLA','available'=>1],
        ['name'=>'QUEIJO MUSSARELA','available'=>1],
        ['name'=>'AZEITONA','available'=>1],
        ['name'=>'PÃO TIPO HAMBURGUER','available'=>1],
        ['name'=>'PÃO TIPO CÃO','available'=>1],
        ['name'=>'BOVINO','available'=>1],
        ['name'=>'ALFACE','available'=>1],
      ];

      foreach($ingredients as $ingredient){
        $new= new Ingredient();
        $new->name=$ingredient['name'];
        $new->available=$ingredient['available'];
        $new->save();
      }    

    }
}
