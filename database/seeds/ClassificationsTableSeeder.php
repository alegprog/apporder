<?php

use Illuminate\Database\Seeder;
use App\Entities\Classification;
use App\Entities\Subclassification;

class ClassificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $classifications=[
        ['name'=>'PIZZA'],
        ['name'=>'COZINHA'],
        ['name'=>'BAR'],
      ];

      $subclassifications=[
        ['name'=>'BROTINHO','abbreviated'=>'BT','quantity'=>1,'classification_id'=>1],
        ['name'=>'MÉDIA','abbreviated'=>'MD','quantity'=>2,'classification_id'=>1],
        ['name'=>'GIGANTE','abbreviated'=>'GG','quantity'=>3,'classification_id'=>1],
        ['name'=>'UN','abbreviated'=>'UN','quantity'=>1,'classification_id'=>2],
        ['name'=>'UN','abbreviated'=>'UN','quantity'=>1,'classification_id'=>3],
      ];
        //Log::info($classifications);
      foreach($classifications as $classification){
        $new= new Classification();
        $new->name=$classification['name'];
        $new->save();
      }

      foreach($subclassifications as $subclassification){
        $new= new Subclassification();
        $new->name=$subclassification['name'];
        $new->abbreviated=$subclassification['abbreviated'];
        $new->quantity=$subclassification['quantity'];
        $new->classification_id=$subclassification['classification_id'];
        $new->save();
      }

    }
}
