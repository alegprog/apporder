<?php

use Illuminate\Database\Seeder;
use App\Entities\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $categories=[
        ['name'=>'FABRICAÇÃO PRÓPRIA'],
        ['name'=>'INDUSTRIALIZADO'],
      ];

      foreach($categories as $category){
        $new= new Category();
        $new->name=$category['name'];
        $new->save();
      }
    }
}
