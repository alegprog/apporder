<?php

use Illuminate\Database\Seeder;
use App\Entities\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $groups=[
        ['name'=>'PIZZA','category_id'=>1],
        ['name'=>'SANDUÍCHE','category_id'=>1],
        ['name'=>'BEBIDAS','category_id'=>2],
      ];

      foreach($groups as $group){
        $new= new Group();
        $new->name=$group['name'];
        $new->category_id=$group['category_id'];
        $new->save();
      }

    }
}
