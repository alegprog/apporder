<?php

use Illuminate\Database\Seeder;
use App\Entities\Subgroup;

class SubgroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $subgroups=[
        ['name'=>'PROMOCIONAIS','group_id'=>1],
        ['name'=>'TRADICIONAIS','group_id'=>1],
        ['name'=>'ESPECIAIS','group_id'=>1],
        ['name'=>'GURMET','group_id'=>1],
        ['name'=>'REDONDO','group_id'=>2],
        ['name'=>'LARGO','group_id'=>2],
        ['name'=>'SODA','group_id'=>3],
        ['name'=>'AGUA','group_id'=>3],
      ];

      foreach($subgroups as $subgroup){
        $new= new Subgroup();
        $new->name=$subgroup['name'];
        $new->group_id=$subgroup['group_id'];
        $new->save();
      }
    }
}
