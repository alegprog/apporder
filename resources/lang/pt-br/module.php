<?php
return [
  'classification'=>'Classificação',
  'category'=>'Categoria',
  'group'=>'Grupo',
  'subgroup'=>'Sub Grupo',
  'product'=>'Produto',
  'ingredient'=>'Ingredient',
  'additional'=>'Adicional',
  'dashboard'=>'Dashboard',
  'menu'=>'Menu',
  'order'=>'Ordem',
];
