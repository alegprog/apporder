<?php
return [
  'add'=>'Adicionar',
  'create'=>'Crio',
  'new'=>'Novo',
  'list'=>'Lista',
  'edit'=>'Editar',
  'update'=>'Atualizar',
  'save'=>'Salvar',
  'delete'=>'Excluir',
  'remove'=>'Excluir',
  'cancel'=>'Cancelar',
  'logout'=>'Sair',
  'login'=>'Entrar',
];
