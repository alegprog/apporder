@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="{{url('/')}}">{!! trans('icon.order') !!} Order</a></li>
        </ol>

        <div class="col-md-12">
         <div class="row">
           <div class="panel panel-default">
            <div class="panel-heading"> {!! trans('icon.order') !!} Order </div>
              <div class="panel-body">
                <div class="col-md-3 col-sm-4">
                  <div class="list-group">
                    @foreach ($groups as $group)
                      <a href="{{route('order.group',['group'=>$group->id])}}" class="list-group-item">{!! trans('icon.right') !!} {{$group->name}}</a>
                    @endforeach
                  </div>
                </div>

                <div class="col-sm-9">
                  <ol class="breadcrumb">
                    <li> <a href="{{route('order.group',['product'=>$selectsubgroup->group->id])}}"><i class="fa fa-angle-right"></i> {{$selectsubgroup->group->name}}</a></li>
                    <li class="active"> {{$selectsubgroup->name}}</li>
                  </ol>

                  <div class="row">

                    @foreach ($selectsubgroup->products as $item)
                    <div class="col-sm-6 col-md-4">
                      <div class="list-group">
                        <a class="list-group-item" href="{{route('order.product',['product'=>$item->id])}}">
                          <div class="text-center">
                            <img src="{{ asset('storage/'.$item->image) }}" id="image-preview" class="img-thumbnail img-responsive" alt="Imagem ..." />
                          </div>

                          <b>{{$item->description}}</b> -
                          @foreach ($item->ingredients as $ingredient)
                            @if ($loop->first)
                                {{$ingredient->name}},
                            @elseif ($loop->last)
                                {{$ingredient->name}}.
                            @else
                                {{$ingredient->name}},
                            @endif
                          @endforeach

                          <div class="clearfix"></div>
                          <label class="text-danger">PREPARO {{$item->preparation}}</label>
                          <div class="clearfix"></div>
                          <label class="text-danger pull-right">R$ {{$item->price}}</label>
                          <div class="clearfix"></div>

                        </a>
                      </div>
                    </div>
                    @endforeach

                  </div>

                </div>

              </div>
            </div>
         </div>
        </div>
      </div>
  </div>
@endsection
