@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="{{url('/')}}">{!! trans('icon.order') !!} Order</a></li>
        </ol>

        <div class="col-md-12">
         <div class="row">
           <div class="panel panel-default">
            <div class="panel-heading"> {!! trans('icon.order') !!} Order </div>
              <div class="panel-body">
                <div class="col-md-3 col-sm-4">
                  <div class="list-group">
                    @foreach ($groups as $group)
                      <a href="{{route('order.group',['group'=>$group->id])}}" class="list-group-item">{!! trans('icon.right') !!} {{$group->name}}</a>
                    @endforeach
                  </div>
                </div>

                <div class="col-sm-9">
                  <ol class="breadcrumb">
                    <li class="active"> <i class="fa fa-angle-right"></i> {{$selectgroup->name}}</a></li>
                  </ol>

                  <div class="row">

                    @foreach ($selectgroup->subgroups as $item)
                    <div class="col-sm-6 col-md-4">
                      <div class="list-group">
                        <a class="list-group-item" href="{{route('order.product',['product'=>$item->id])}}"> <i class="fa fa-circle"></i> {{$item->name}} </a>
                      </div>
                    </div>
                    @endforeach

                  </div>

                </div>

              </div>
            </div>
         </div>
        </div>
      </div>
  </div>
@endsection
