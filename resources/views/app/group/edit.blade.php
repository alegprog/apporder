@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li><a href="{{route('group')}}">{{trans('module.group')}}</a></li>
        <li class="active">{{trans('action.edit')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.update')!!} {{trans('action.edit')}}</div>

                <div class="panel-body">

                  {!! Form::model($group,['route' => ['group.update', $group->id],'class'=>'form-horizontal']) !!}
                    {{ method_field('PUT') }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                          {!! Form::text('name', null,['tabindex'=>'1','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.name').' da '.trans('module.group')]) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                        {!! Form::label('category_id', trans('label.category_id'), ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                          {!!Form::select('category_id', array_pluck($categories,'name','id'), null, ['tabindex'=>'2','placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control'])!!}
                            @if ($errors->has('category_id'))
                              <span class="help-block">
                                <strong>{{ $errors->first('category_id') }}</strong>
                              </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              {!! Form::hidden('id', null) !!}
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-red btn-l','type'=>'submit']) !!}
                              {!! link_to_route('group', trans('action.cancel'), $parameters = [], ['class'=>'btn btn-primary btn-l']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/jquery.ya-enter2tab.js') }}" type="text/javascript"></script>

    <script type="text/javascript" >
        $("form").enableEnterToTab({ captureTabKey: true });
    </script>
@endsection
