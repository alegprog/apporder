@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
  <div class="col-md-6 col-md-offset-3 col-sm-6 col-md-offset-3">
   <div class="panel panel-default">
    <div class="panel-heading"> Registro Perfil Google</div>
      <div class="panel-body">

      <form method="post" action="/auth/google/register">
        {{ csrf_field() }}
        <div class="card">
          <div class="card-block text-center">
            <img class="img-thumbnail" width="80" src="{{ $user->avatar }}">
          </div>
          <hr>
          <div class="card-block">
            <div class="form-group">
              <label for="name" class="form-control-label">
                {{trans('label.name')}}
              </label>
              <input class="form-control" type="text" name="name" value="{{ $user->name }}" readonly>
            </div>

            <div class="form-group">
              <label for="email" class="form-control-label">
                {{trans('label.email')}}
              </label>
              <input class="form-control" type="text" name="email" value="{{ $user->email }}" readonly>
            </div>
          </div>

          <div class="card-footer">
            <button class="btn btn-red btn-l" type="submit">
              {{trans('action.save')}}
            </button>
          </div>
        </div>

      </form>
     </div>
    <div>
   </div>
  </div>
</div>
@endsection
