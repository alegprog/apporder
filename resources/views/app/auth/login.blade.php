@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{!! trans('icon.login') !!} {{trans('action.login')}}</div>

                <div class="panel-body">

                  <div class="col-sm-6">
                      {!! Html::decode(link_to_route('auth.google', trans('icon.google').' '.'Entrar con Google', $parameters = [], ['class'=>'btn btn-red btn-l btn-block'])) !!}
                  </div>
                  <div class="col-sm-6">
                      {!! Html::decode(link_to_route('auth.facebook', trans('icon.facebook').' '.'Entrar con Facebook', $parameters = [], ['class'=>'btn btn-primary btn-l btn-block'])) !!}
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
