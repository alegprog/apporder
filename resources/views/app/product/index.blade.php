@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li class="active">{{trans('module.product')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <i class="glyphicon glyphicon-ok"></i> <strong> {{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
      <div class="col-md-12 p-b-10">
        <div class="row">
          {!! Html::decode(link_to_route('product.create', trans('icon.new').' '.trans('action.new'), $parameters = [], ['class'=>'btn btn-primary btn-l pull-right'])) !!}
        </div>
      </div>
        <div class="col-md-12">
          <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.list')!!} {{trans('action.list')}}</div>

                <div class="panel-body">

                    <table class="table table-striped">
                      <thead>
                       <tr>
                        <th>
                          {{ trans('label.description') }}
                        </th>
                        <th>
                          {{ trans('label.available') }}
                        </th>
                        <th>
                          {{ trans('label.classification') }}
                        </th>
                        <th>
                          {{ trans('label.category') }}
                        </th>
                        <th class="width-200 text-center" >
                          {{ trans('label.action') }}
                        </th>
                       </tr>
                      </thead>
                      <tbody>
                        @foreach ($products as $product)
                        <tr>
                          <td>{{$product->description}}</td>
                          <td>{{$product->availability}}  {{-- $product->availability --}}</td>
                          <td>{{$product->classification->name}}</td>
                          <td>{{$product->category->name}}</td>
                          <td class="text-right">
                            {!! Form::open(['route' => ['product.delete',$product->id],'class'=>'','name'=>'frm-'.$product->id,'id'=>'frm-'.$product->id ]) !!}
                            {!! Html::decode(link_to_route('product.edit', trans('icon.edit').' '.trans('action.edit'), ['id'=>$product->id],['class'=>'btn btn-primary'])) !!}
                              {!! method_field('delete') !!}
                              <button type="submit" onclick="deleteProduct(this,event)" class="btn btn-red btn-delete">
                                {!!trans('icon.delete')!!} {{trans('action.delete')}}
                              </button>
                              {!! Form::hidden('id',$product->id) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>


                </div>


            </div>
          </div>
        </div>
    </div>
</div>
@endsection
@section('script')
 <script type="text/javascript" >
 function deleteProduct(el,e){
         e.preventDefault();
         if (!confirm("Tem certeza de que deseja excluir?")) {
             return false;
         }
         //return true;
         var row     = $(el).parents('tr');
         var form    = $(el).parents('form');
         var url     = form.attr('action');

         form.submit();
 }
 </script>
@endsection
