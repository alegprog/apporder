@extends('layouts.app')

@section('content')
@if (count($errors) > 0)
  {{--dd(old())--}}
  @if(!$errors->has('group_id'))
    @php
        $group_id= old('group_id') ? old('group_id') : '';
        $listsubgroup=App\Entities\Group::with(['subgroups' => function ($query){
        }])->find($group_id);
    @endphp
 @endif
 @if(!$errors->has('category_id'))
   @php
       $category_id= old('category_id') ? old('category_id') : '';
       $listgroup=App\Entities\Category::with(['groups' => function ($query){
       }])->find($category_id);
   @endphp
 @endif

@endif
@php
  $next=0;
  $next_additional=0;
  $placeholder_select='--- '.trans('label.select').' ---';
  //print_r(old());
  //print_r(session('errors'));
@endphp
<div class="container">
  <div class="col-md-12">
    <div class="row">

      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li><a href="{{route('product')}}">{{trans('module.product')}}</a></li>
        <li class="active">{{trans('action.new')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-12">
            <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.store')!!} {{trans('action.new')}}</div>

                <div class="panel-body">

                  {!! Form::open(['route' => 'product.store','class'=>'form-horizontal','enctype' => 'multipart/form-data','data-plus-as-tab'=>'true']) !!}
                    {{ csrf_field() }}

                    <div class="col-sm-9 col-md-9">

                      <div class="row">

                      <div class="col-md-6">

                          <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">

                          {!! Form::label('code', trans('label.code'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('code', null,['disabled'=>'disabled',  'class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.code').' da '.trans('module.product')]) !!}

                              @if ($errors->has('code'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('code') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>

                      </div>

                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          {!! Form::label('description', trans('label.description'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('description', null,['class' => 'form-control', 'tabindex'=>'1', 'autofocus'=>'autofocus', 'data-plus-as-tab'=>'true', 'placeholder'=>trans('label.description').' da '.trans('module.product')]) !!}

                              @if ($errors->has('description'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('description') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-4">
                        <div class="form-group{{ $errors->has('barcode') ? ' has-error' : '' }}">
                          {!! Form::label('barcode', trans('label.barcode'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('barcode', null,['class' => 'form-control', 'tabindex'=>'2', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.barcode')]) !!}

                              @if ($errors->has('barcode'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('barcode') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group{{ $errors->has('available') ? ' has-error' : '' }}">
                          {!! Form::label('available', trans('label.available'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!!Form::select('available', array_pluck($availables,'name','id'), null, ['tabindex'=>'3','placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control'])!!}
                              @if ($errors->has('available'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('available') }}</strong>
                                </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group{{ $errors->has('order') ? ' has-error' : '' }}">
                          {!! Form::label('order', trans('label.order'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('order', null,['class' => 'form-control', 'tabindex'=>'4', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.order')]) !!}

                              @if ($errors->has('order'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('order') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                    </div>

                    </div>
                    <div class="col-sm-3 col-md-3">
                      <div class="row">
                        <img src="{{ asset('image/default/default-thumbnail.jpg') }}" id="image-preview" class="img-responsive" alt="Imagem ..." />
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          {!! Form::label('image', trans('label.image'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::file('image',['id'=>'image','class' => 'form-control', 'tabindex'=>'5','placeholder'=>trans('label.image')]) !!}

                              @if ($errors->has('image'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('image') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                        {{--<input type="file" name="image" id="image">--}}

                      </div>
                    </div>



                    <div class="col-sm-12 p-t-20">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                          <a href="#definitions" aria-controls="definitions" role="tab" data-toggle="tab">
                            @if ($errors->has('classification_id') || $errors->has('category_id') || $errors->has('group_id') || $errors->has('subgroup_id'))
                              <span class="glyphicon glyphicon-asterisk color-red"></span>
                            @endif
                            {{trans('label.definitions')}}
                          </a>
                        </li>
                        <li role="presentation">
                          <a class="bold" href="#ingredients" aria-controls="ingredients" role="tab" data-toggle="tab">
                            {{trans('label.ingredients')}}
                          </a>
                        </li>
                        <li role="presentation">
                          <a href="#additional" aria-controls="additional" role="tab" data-toggle="tab">
                            @if ($errors->has('additionals.*.name') || $errors->has('additionals.*.value') || $errors->has('additionals.*.default'))
                              <span class="glyphicon glyphicon-asterisk color-red"></span>
                            @endif
                            {{trans('label.additional')}}

                          </a>
                        </li>
                        <li role="presentation">
                          <a href="#others" aria-controls="others" role="tab" data-toggle="tab">
                          @if ($errors->has('preparation') || $errors->has('cost') || $errors->has('moneymaking') || $errors->has('price'))
                            <span class="glyphicon glyphicon-asterisk color-red"></span>
                          @endif
                            {{trans('label.others')}}
                          </a>
                        </li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="definitions">
                          <div class="col-md-12 p-t-20"></div>

                          <div class="form-group{{ $errors->has('classification_id') ? ' has-error' : '' }}">
                              {!! Form::label('classification_id', trans('label.classification_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('classification_id', array_pluck($classifications,'name','id'), null, ['tabindex'=>'6','placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control'])!!}
                                  @if ($errors->has('classification_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('classification_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                              {!! Form::label('category_id', trans('label.category_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('category_id', array_pluck($categorys,'name','id'), null, ['tabindex'=>'7','placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control','id'=>'slt-category'])!!}
                                  @if ($errors->has('category_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                              {!! Form::label('group_id', trans('label.group_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('group_id', old('category_id') ? array_pluck($listgroup->groups,'name','id') : [], null, ['tabindex'=>'8','placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control','id'=>'slt-group'])!!}
                                  @if ($errors->has('group_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('group_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('subgroup_id') ? ' has-error' : '' }}">
                              {!! Form::label('subgroup_id', trans('label.subgroup_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('subgroup_id', old('group_id') ? array_pluck($listsubgroup->subgroups,'name','id') : [], null, ['tabindex'=>'9','placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control','id'=>'slt-subgroup'])!!}
                                  @if ($errors->has('subgroup_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('subgroup_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="ingredients">
                          <div class="col-md-12 p-t-20"></div>

                          <div class="form-group after-add-ingredent">
                            <div class="col-xs-12">
                              <div class="p-l-10">
                                <button class="btn btn-success add-ingredent"  type="button"><i class="glyphicon glyphicon-plus"></i> {{trans('action.add')}}</button>
                              </div>
                            </div>
                          </div>

                          @if(old('ingredients') && count(old('ingredients')>0))
                            @foreach (old('ingredients') as $key => $value)
                              @php
                                if($key>$next){
                                 $next=$key;
                                }
                              @endphp


                            <div class="control-group-input" style="margin-top:3px">
                              <div class="row">

                            <div class="col-xs-12">{{--<hr>--}}</div>

                            <div class="col-sm-6 col-md-6">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 {!!Form::select('ingredients['.$key.'][name]', array_pluck($ingredients,'name','code'), old("ingredients.$key.name"), ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control slt-ingredients','data-key'=>$key,'id'=>'ingredients_'.$key.'_name'])!!}
                                 {{--<input type="text" name="ingredients[{{$key}}][name]" class="form-control" placeholder="{{trans('label.name')}} da {{trans('label.ingredient')}}" value="{{old("ingredients.$key.name")}}">--}}
                                 {!! Form::hidden('ingredients['.$key.'][id]', old("ingredients.$key.id"), ['id'=>'ingredient_'.$key.'_id']) !!}
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-4 col-md-4">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 {{--<input type="text" id="ingredient_{{$key}}_visible" name="ingredients[{{$key}}][visible]" class="form-control" value="{{old("ingredients.$key.visible")}}" >--}}
                                 {!!Form::select("ingredients[$key][visible]",array_pluck($status_ingredients,'name','id'), null, ['placeholder'=>'','class'=>'form-control','readonly'=>'readonly','id'=>'ingredient_'.$key.'_visible' ])!!}
                                 {{--<select id="ingredients_{{$key}}_visible" name="ingredients[{{$key}}][visible]" class="form-control"><option value="1">Visible</option><option value="0">Invisible</option></select>--}}
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-2 col-md-2">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('action.remove')}}</button>
                                </div>
                              </div>
                            </div>

                            <div class="clearfix"></div>

                            </div>
                            </div>
                            @endforeach
                        @endif



                        </div>

                        <!-- Init Additional -->
                        <div role="tabpanel" class="tab-pane" id="additional">

                          <div class="col-md-12 p-t-20"></div>

                            <div class="form-group after-add-additional">
                              <div class="col-xs-12">
                                <div class="p-l-10">
                                  <button class="btn btn-success add-additional" type="button"><i class="glyphicon glyphicon-plus"></i> {{trans('action.add')}}</button>
                                </div>
                              </div>
                            </div>

                            <!-- Init Additional old-->
                            @if(old('additionals') && count(old('additionals')>0))
                              @foreach (old('additionals') as $key => $value)
                                @php
                                  if($key>$next_additional){
                                   $next_additional=$key;
                                  }
                                @endphp


                              <div class="control-group-input" style="margin-top:3px">
                                <div class="row">

                              <div class="col-xs-12">{{--<hr>--}}</div>

                              <div class="col-sm-5 col-md-5">
                                <div class="form-group {{ $errors->has("additionals.$key.name") ? ' has-error' : '' }}">
                                  <label for="additional_{{$key}}_name" class="col-sm-12 c-label">{{trans('label.name')}}</label>
                                  <div class="col-sm-12">
                                   {{--<input type="text" id="additional_{{$key}}_name" name="additionals[{{$key}}][name]" class="form-control" placeholder="" value="{{old("additionals.$key.name")}}" >--}}
                                   {!!Form::select('additionals['.$key.'][name]', array_pluck($additionals,'name','code'), old("additionals.$key.name"), ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control slt-additionals','data-key'=>$key,'id'=>'additionals_'.$key.'_name'])!!}
                                   {!! Form::hidden('additionals['.$key.'][id]', old("additionals.$key.id"), ['id'=>'additional_'.$key.'_id']) !!}
                                   @if ($errors->has("additionals.$key.name"))
                                       <span class="help-block">
                                           <strong>{{ $errors->first("additionals.$key.name") }}</strong>
                                       </span>
                                   @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-sm-3 col-md-3">
                                <div class="form-group {{ $errors->has("additionals.$key.value") ? ' has-error' : '' }}">
                                  <label for="additional_{{$key}}_value" class="col-sm-12 c-label">{{trans('label.value')}}</label>
                                  <div class="col-sm-12">
                                   <input type="text" readonly="readonly" id="additional_{{$key}}_value" name="additionals[{{$key}}][value]" class="form-control additional_value" placeholder="" value="{{old("additionals.$key.value")}}">
                                   @if ($errors->has("additionals.$key.value"))
                                       <span class="help-block">
                                           <strong>{{ $errors->first("additionals.$key.value") }}</strong>
                                       </span>
                                   @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-sm-2 col-md-2">
                                <div class="form-group {{ $errors->has("additionals.$key.default") ? ' has-error' : '' }}">
                                  <label for="additional_{{$key}}_default" class="col-sm-12 c-label">{{trans('label.default')}}</label>
                                  <div class="col-sm-12">
                                  <input type="text" readonly="readonly" id="additional_{{$key}}_default" name="additionals[{{$key}}][default]" class="form-control" placeholder="" value="{{old("additionals.$key.default")}}">
                                  {{--<label class="radio-inline">
                                    <input type="radio" name="additionals[{{$key}}][default]" id="additional_{{$key}}_default1" value="1" {{old("additionals.$key.default")==1 ? 'checked' : ''}}> {{trans('label.yes')}}
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="additionals[{{$key}}][default]" id="additional_{{$key}}_default2" value="0" {{old("additionals.$key.default")==0 ? 'checked' : ''}} > {{trans('label.no')}}
                                  </label>--}}
                                  @if ($errors->has("additionals.$key.default"))
                                      <span class="help-block">
                                          <strong>{{ $errors->first("additionals.$key.default") }}</strong>
                                      </span>
                                  @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-sm-2 col-md-2">
                                <div class="form-group">
                                  <div class="col-sm-12">
                                   <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('action.remove')}}</button>
                                  </div>
                                </div>
                              </div>

                              <div class="clearfix"></div>

                              </div>
                              </div>
                              @endforeach
                          @endif
                          <!-- End Additional old -->


                        </div>
                        <!-- End Additional -->

                        <div role="tabpanel" class="tab-pane" id="others">
                          <div class="col-md-12 p-t-20"></div>

                          <div class="col-sm-offset-8 col-md-4">
                            <div class="form-group{{ $errors->has('preparation') ? ' has-error' : '' }}">
                              {!! Form::label('preparation', trans('label.preparation'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('preparation', null,['tabindex'=>'10','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.preparation')]) !!}

                                  @if ($errors->has('preparation'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('preparation') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                              {!! Form::label('cost', trans('label.cost'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('cost', null,['tabindex'=>'11','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.cost'),'id'=>'cost','maxlength'=>'15', 'autocomplete'=>'off']) !!}

                                  @if ($errors->has('cost'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('cost') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group{{ $errors->has('moneymaking') ? ' has-error' : '' }}">
                              {!! Form::label('moneymaking', trans('label.moneymaking'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('moneymaking', null,['tabindex'=>'12','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.moneymaking'),'id'=>'moneymaking','maxlength'=>'8', 'autocomplete'=>'off']) !!}

                                  @if ($errors->has('moneymaking'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('moneymaking') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                              {!! Form::label('price', trans('label.price'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('price', null,['tabindex'=>'13','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.price'),'id'=>'price','maxlength'=>'15', 'autocomplete'=>'off']) !!}

                                  @if ($errors->has('price'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('price') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                    {{--<div class="col-sm-12">
                      <br><br><br>
                      <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                          {!! Form::label('group_id', trans('label.group'), ['class' => 'col-md-4 control-label']) !!}
                          <div class="col-md-6">
                            {!!Form::select('group_id', array_pluck($groups,'name','id'), null, ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control'])!!}
                              @if ($errors->has('group_id'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('group_id') }}</strong>
                                </span>
                              @endif
                          </div>
                      </div>
                    </div>--}}


                    <div class="col-sm-12 p-t-20">
                     <hr>
                     <div class="form-group">
                          <div class="col-md-6">
                              {!! Form::button(trans('action.save'),['tabindex'=>'14','class'=>'btn btn-red btn-l','type'=>'submit']) !!}
                              {!! link_to_route('product', trans('action.cancel'), $parameters = [], ['class'=>'btn btn-primary btn-l']) !!}
                          </div>
                     </div>
                    </div>
                  {!! Form::close() !!}

                </div>
            </div>

          </div>

        </div>
    </div>
</div>

</div>
@endsection

@section('script')
 <script src="{{ asset('vendors/inputmask/inputmask.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/jquery.inputmask.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.date.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
 {{--<script src="{{ asset('js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
 <script src="{{ asset('js/jquery.simulate.js') }}" type="text/javascript"></script>
 <script src="{{ asset('js/plusastab.joelpurra.js') }}" type="text/javascript"></script> --}}
 <script src="{{ asset('js/jquery.ya-enter2tab.js') }}" type="text/javascript"></script>



 <script type="text/javascript" >

 $("form").enableEnterToTab({ captureTabKey: true });

 /*
 JoelPurra.PlusAsTab.setOptions({
		// Use enter instead of plus
		// Number 13 found through demo at
		// https://api.jquery.com/event.which/
		key: 13
	});

  */


 /*var $templateInput=`<div class="control-group input-group" style="margin-top:10px">
   <input type="text" name="ingredients[:next:]['name']" class="form-control" placeholder="Nome ingrediente">
   <input type="text" name="ingredients[:next:]['visible']" class="form-control" placeholder="Nome ingrediente">
   <div class="input-group-btn">
     <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
   </div>
 </div>`*/

 var $templateInput=`<div class="control-group-input" style="margin-top:3px">
<div class="row">
<div class="col-xs-12"></div>

 <div class="col-sm-6 col-md-6">
   <div class="form-group">
     <div class="col-sm-12">
      {{--<input type="text" name="ingredients[:next:][name]" class="form-control" placeholder=":placeholder:">--}}
      <select id="ingredients_:next:_name" name="ingredients[:next:][name]" placeholder="{{$placeholder_select}}" class="form-control slt-ingredients" data-key=":next:">
      <option value="">{{$placeholder_select}}</option>
      @foreach($ingredients as $ingredient)
        {{--dd($ingredient['code'])--}}
        <option value="{{$ingredient->code}}">{{$ingredient->name}}</option>
      @endforeach
      </select>
      <input type="hidden" id="ingredient_:next:_id" name="ingredients[:next:][id]">
     </div>
   </div>
 </div>

 <div class="col-sm-4 col-md-4">
   <div class="form-group">
     <div class="col-sm-12">
       {{--<input type="text" id="ingredient_:next:_visible" name="ingredients[:next:][visible]" class="form-control">--}}
      <select id="ingredient_:next:_visible" readonly="readonly" name="ingredients[:next:][visible]" class="form-control"><option value=""></option><option value="1">:visible:</option><option value="0">:invisible:</option></select>
     </div>
   </div>
 </div>

 <div class="col-sm-2 col-md-2">
   <div class="form-group">
     <div class="col-sm-12">
      <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> :action_remove:</button>
     </div>
   </div>
 </div>

 <div class="clearfix"></div>
 </div>
 </div>`;

 var $templateInputAdditional=`<div class="control-group-input" style="margin-top:3px">
<div class="row">
<div class="col-xs-12"></div>

 <div class="col-sm-5 col-md-5">
   <div class="form-group">
     <label for="additional_:next:_name" class="col-sm-12 c-label">:label_name:</label>
     <div class="col-sm-12">
      {{--<input type="text" id="additional_:next:_name" name="additionals[:next:][name]" class="form-control" placeholder="">--}}
      <select id="additional_:next:_name" name="additionals[:next:][name]" placeholder="{{$placeholder_select}}" class="form-control slt-additionals" data-key=":next:">
      <option value="">{{$placeholder_select}}</option>
      @foreach($additionals as $additional)
        <option value="{{$additional->code}}">{{$additional->name}}</option>
      @endforeach
      </select>
      <input type="hidden" id="additional_:next:_id" name="additionals[:next:][id]">
     </div>
   </div>
 </div>

 <div class="col-sm-3 col-md-3">
   <div class="form-group">
     <label for="additional_:next:_value" class="col-sm-12 c-label">:label_value:</label>
     <div class="col-sm-12">
      <input type="text" id="additional_:next:_value" readonly="readonly" name="additionals[:next:][value]" class="form-control additional_value" placeholder="">
     </div>
   </div>
 </div>

 <div class="col-sm-2 col-md-2">
   <div class="form-group">
     <label for="additional_:next:_default" class="col-sm-12 c-label">:label_default:</label>
     <div class="col-sm-12">
     <input type="text" id="additional_:next:_default" readonly="readonly" name="additionals[:next:][default]" class="form-control" placeholder="">
     {{--<label class="radio-inline">
       <input type="radio" name="additionals[:next:][default]" id="additional_:next:_default1" value="1"> :label_yes:
     </label>
     <label class="radio-inline">
       <input type="radio" name="additionals[:next:][default]" id="additional_:next:_default2" value="0" checked> :label_no:
     </label>--}}
     </div>
   </div>
 </div>

 <div class="col-sm-2 col-md-2">
   <div class="form-group">
     <div class="col-sm-12">
      <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> :action_remove:</button>
     </div>
   </div>
 </div>

 <div class="clearfix"></div>
 </div>
 </div>`;

$action_remove="{{trans('action.remove')}}";
$label_invisible="{{trans('label.invisible')}}";
$label_visible="{{trans('label.visible')}}";
$placeholder_ingredient="{{trans('label.name')}}"+' de '+"{{trans('label.ingredient')}}";
$label_yes= "{{trans('label.yes')}}";
$label_no = "{{trans('label.no')}}";
$label_value = "{{trans('label.value')}}";
$label_name = "{{trans('label.name')}}";
$label_default = "{{trans('label.default')}}";


 $(document).ready(function(){
   var next = {{$next}}+1;
   var next_additional = {{$next_additional}}+1;
  $("#preparation").inputmask("99:99:99");
  $("#cost").inputmask({
    alias: 'decimal',
    groupSeparator: '',
    autoGroup: true,
    digits: 2,
    radixPoint: ',',
    placeholder: '0',
    digitsOptional: false
  });
  $("#moneymaking").inputmask({
    alias: 'decimal',
    groupSeparator: '',
    autoGroup: true,
    digits: 2,
    radixPoint: ',',
    placeholder: '0',
    digitsOptional: false
  });
  $("#price").inputmask({
    alias: 'decimal',
    groupSeparator: '',
    autoGroup: true,
    digits: 2,
    radixPoint: ',',
    placeholder: '0',
    digitsOptional: false
  });


  //$("#cost").inputmask('9{1,8},9{0,2}');
  //$("#moneymaking").inputmask('9{1,8},9{1,2}');
  //$("#price").inputmask('9{1,8},9{1,2}');
  //$(".additional_price").inputmask("999999999.99");

  $(".add-ingredent").click(function(){
      var html = $templateInput
      .replace(/:next:/g, next)
      .replace(':action_remove:',$action_remove)
      .replace(':invisible:',$label_invisible)
      .replace(':visible:',$label_visible)
      .replace(':placeholder:',$placeholder_ingredient);
      next = next + 1;
      $(".after-add-ingredent").after(html);
  });

  $("body").on("click",".remove-ingredent",function(){
      if (!confirm("Tem certeza de que deseja excluir?")) {
          return false;
      }
      $(this).parents(".control-group-input").fadeOut()
      $(this).parents(".control-group-input").remove();
  });

  $(".add-additional").click(function(){
      var html = $templateInputAdditional
      .replace(/:next:/g, next_additional)
      .replace(':action_remove:',$action_remove)
      .replace(':label_name:',$label_name)
      .replace(':label_default:',$label_default)
      .replace(':label_value:',$label_value)
      .replace(':label_yes:',$label_yes)
      .replace(':label_no:',$label_no);
      next_additional = next_additional + 1;
      $(".after-add-additional").after(html);
  });

  $("body").on("change",".slt-ingredients",function(){
    $key=$(this).data('key');
    $value=$(this).val();
    $empty='';
    if(!$value==''){
      var $ingredient=$value.split("|");
      var $visible = $ingredient[0] ? 'V' : 'I';
      $('#ingredient_'+$key+'_id').val($ingredient[0]);
      //$('#ingredient_'+$key+'_visible select').val($ingredient[1]);
      //$('#ingredient_'+$key+'_visible').val($visible);
      $('#ingredient_'+$key+'_visible option[value='+$ingredient[1]+']').prop("selected",true);
    }else{
      $('#ingredient_'+$key+'_id').val('');
      //$('#ingredient_'+$key+'_visible').val('');
      $('select#ingredient_'+$key+'_visible').prop('selectedIndex', 0);
      //$('#ingredient_'+$key+'_visible select').val('');
      //$('#ingredient_'+$key+'_visible option[value=]').attr('selected','selected');
    }

  });

  $("body").on("change",".slt-additionals",function(){
    $key=$(this).data('key');
    $value=$(this).val();
    $empty='';
    if(!$value==''){
      var $additional=$value.split("|");
      var $default = $additional[2]==1 ? 'Sim' : 'Não';
      var $price = eval($additional[1]).toFixed(2);
      $('#additional_'+$key+'_id').val($additional[0]);
      $('#additional_'+$key+'_value').val($price.replace('.',','));
      $('#additional_'+$key+'_default').val($default);
    }else{
      $('#ingredient_'+$key+'_id').val('');
      $('#additional_'+$key+'_value').val('');
      $('#additional_'+$key+'_default').val('');
    }

  });

});

$("#slt-category").change(function(){

         var group = $("#slt-group");
         var subgroup = $("#slt-subgroup");

         // Guardamos el select de alumnos
         var category = $(this);
         //console.log(state.val());
         var url = base_url+'/dashboard/category/group';
         $token=$('meta[name="csrf-token"]').attr('content');
         console.log($token);
         if($(this).val() != '')
         {
             $.ajax({
                 data: { id : category.val()},
                 url:  url,
                 type:  'POST',
                 dataType: 'json',
                 beforeSend: function ()
                 {
                     category.prop('disabled', true);
                     group.prop('disabled', true);
                     subgroup.find('option').remove();
                     subgroup.prop('disabled', true);
                 },
                 success:  function (r)
                 {
                     category.prop('disabled', false);

                     // Limpiamos el select
                     group.find('option').remove();
                     group.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
                     $(r.groups).each(function(i, v){ // indice, valor
                         group.append('<option value="' + v.id + '">' + v.name + '</option>');
                     })

                     group.prop('disabled', false);
                 },
                 error: function(r)
                 {
                     //console.log(r);
                     //alert('Ocurrio un error en el servidor ..');
                     group.find('option').remove();
                     category.prop('disabled', false);
                 }
             });
         }else
         {
             group.find('option').remove();
             group.prop('disabled', true);
             subgroup.find('option').remove();
             subgroup.prop('disabled', true);
         }
     });
  $("#slt-group").change(function(){

           var subgroup = $("#slt-subgroup");

           // Guardamos el select de alumnos
           var group = $(this);
           //console.log(state.val());
           var url = base_url+'/dashboard/group/subgroup';
           $token=$('meta[name="csrf-token"]').attr('content');
           console.log($token);
           if($(this).val() != '')
           {
               $.ajax({
                   data: { id : group.val()},
                   url:  url,
                   type:  'POST',
                   dataType: 'json',
                   beforeSend: function ()
                   {
                       group.prop('disabled', true);
                       subgroup.prop('disabled', true);
                   },
                   success:  function (r)
                   {
                       group.prop('disabled', false);

                       // Limpiamos el select
                       subgroup.find('option').remove();
                       subgroup.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
                       $(r.subgroups).each(function(i, v){ // indice, valor
                           subgroup.append('<option value="' + v.id + '">' + v.name + '</option>');
                       })

                       subgroup.prop('disabled', false);
                   },
                   error: function(r)
                   {
                       //console.log(r);
                       //alert('Ocurrio un error en el servidor ..');
                       subgroup.find('option').remove();
                       group.prop('disabled', false);
                   }
               });
           }else
           {
               subgroup.find('option').remove();
               subgroup.prop('disabled', true);
           }
       });
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
      }
      $("#image").change(function(){
          readURL(this);
      });

      function calcularCost(input){
        var $cost = $("#cost").val().replace(',','.');
        var $porce = $("#moneymaking").val().replace(',','.');
        var $price = $("#price").val().replace(',','.');
        if(!isNaN($cost)) {
          if($cost > 0){
            if(!isNaN($porce)){
              if($porce > 0){
                $price=((eval($cost)*eval($porce))/100)+eval($cost);
                $price=round($price,2);
                $price=$price.toString().replace('.',',');
                $("#price").val($price);
              }else{
                $price='';
                $("#price").val($price);
                /*if(!isNaN($price)){
                  if($price > 0){
                    $porce=((eval($cost)-eval($price))/eval($cost))*100;
                    $porce=$porce*-1;
                    $("#moneymaking").val($porce);
                  }
                }*/
              }
            }else{
              $price='';
              $("#price").val($price);
            }
          }else{
            $price='';
            $("#price").val($price);
          }
        }else{
          $price='';
          $("#price").val($price);
        }
      }


      function calcularMoneymaking(input){
        var $cost = $("#cost").val().replace(',','.');
        var $porce = $("#moneymaking").val().replace(',','.');
        var $price = $("#price").val().replace(',','.');
        if(!isNaN($cost)) {
          if($cost > 0){
            if(!isNaN($porce)){
              if($porce > 0){
                $price=((eval($cost)*eval($porce))/100)+eval($cost);
                $price=round($price,2);
                $price=$price.toString().replace('.',',');
                $("#price").val($price);
              }else{
                $price='';
                $("#price").val($price);
              }
            }else{
              $price='';
              $("#price").val($price);
            }
          }else{
            $price='';
            $("#price").val($price);
          }
        }else{
          $price='';
          $("#price").val($price);
        }
      }

      function calcularPrice(input){
        var $cost = $("#cost").val().replace(',','.');
        var $porce = $("#moneymaking").val().replace(',','.');
        var $price = $("#price").val().replace(',','.');
        if(!isNaN($cost)) {
          if($cost > 0){
            if(!isNaN($price)){
              if($price > 0){
                $porce=((eval($cost)-eval($price))/eval($cost))*100;
                $porce=$porce*-1;
                $porce=round($porce,2);
                $porce=$porce.toString().replace('.',',');
                $("#moneymaking").val($porce);
              }else{
                $price='';
                $("#moneymaking").val($price);
              }
            }else{
              $price='';
              $("#moneymaking").val($price);
            }
          }else{
            $price='';
            $("#moneymaking").val($price);
          }
        }else{
          $price='';
          $("#moneymaking").val($price);
        }
      }

      $("#cost").keyup(function(){
          calcularCost(this);
      });

      $("#moneymaking").keyup(function(){
          calcularMoneymaking(this);
      });

      $("#price").keyup(function(){
          calcularPrice(this);
      });

      function round(value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
      }
 </script>
@endsection
