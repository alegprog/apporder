@extends('layouts.app')

@section('content')
@php
  $category_id = $product->category_id;
  $group_id = $product->group_id;
  $subgroup_id = $product->subgroup_id;
  $classification_id = $product->classification_id;
  if (count($errors) > 0){
    if(!$errors->has('group_id')){
        $group_id= old('group_id') ? old('group_id') : '';
        $subgroups = App\Entities\Subgroup::where('group_id', $group_id)
           ->select('id as id', 'name as name')
           ->get()
           ->toArray();

   }else{
      $subgroups=[];
      $group_id= empty(old('group_id')) ? '' : old('group_id');
   }

   if(!$errors->has('category_id')){
     //dd($category_id);
     $category_id= old('category_id') ? old('category_id') : '';
     $group=App\Entities\Group::where('category_id', $category_id)
        ->select('id as id', 'name as name')
        ->get()
        ->toArray();

   }else{
     $groups=[];
     $category_id= empty(old('category_id')) ? '' : old('category_id');
   }

   if(!$errors->has('subgroup_id')){
     $subgroup_id= old('subgroup_id') ? old('subgroup_id') : '';
   }else{
     $subgroup_id= empty(old('subgroup_id')) ? '' : old('subgroup_id');
   }

   if(!$errors->has('classification_id')){
     $classification_id= old('classification_id') ? old('classification_id') : '';
   }else{
     $classification_id= empty(old('classification_id')) ? '' : old('classification_id');
   }

 }


@endphp
@php
  $next=0;
  $next_additional=0;
  //print_r(old());
@endphp
<div class="container">
  <div class="col-md-12">
    <div class="row">

      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li><a href="{{route('product')}}">{{trans('module.product')}}</a></li>
        <li class="active">{{trans('action.edit')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-12">
            <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.update')!!} {{trans('action.edit')}}</div>

                <div class="panel-body">

                  {!! Form::model($product,['route' => ['product.update',$product->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
                      {{ method_field('PUT') }}

                    <div class="col-sm-9 col-md-9">

                      <div class="row">

                      <div class="col-md-6">

                          <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">

                          {!! Form::label('code', trans('label.code'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('code', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.code').' da '.trans('module.product')]) !!}

                              @if ($errors->has('code'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('code') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>

                      </div>

                      <div class="col-md-6">
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          {!! Form::label('description', trans('label.description'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('description', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.description').' da '.trans('module.product')]) !!}

                              @if ($errors->has('description'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('description') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-4">
                        <div class="form-group{{ $errors->has('barcode') ? ' has-error' : '' }}">
                          {!! Form::label('barcode', trans('label.barcode'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('barcode', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.barcode')]) !!}

                              @if ($errors->has('barcode'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('barcode') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group{{ $errors->has('available') ? ' has-error' : '' }}">
                          {!! Form::label('available', trans('label.available'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!!Form::select('available', array_pluck($availables,'name','id'), null, ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control'])!!}
                              @if ($errors->has('available'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('available') }}</strong>
                                </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group{{ $errors->has('order') ? ' has-error' : '' }}">
                          {!! Form::label('order', trans('label.order'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::text('order', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.order')]) !!}

                              @if ($errors->has('order'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('order') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                    </div>

                    </div>
                    <div class="col-sm-3 col-md-3">
                      <div class="row ">
                        <div class="text-center">
                          <img src="{{ asset('storage/'.$product->image) }}" id="image-preview" class="img-thumbnail img-responsive" alt="Imagem ..." />
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          {!! Form::label('image', trans('label.image'), ['class' => 'col-sm-12 c-label']) !!}

                          <div class="col-sm-12">
                            {!! Form::file('image',['id'=>'image','class' => 'form-control','placeholder'=>trans('label.image')]) !!}

                              @if ($errors->has('image'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('image') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                        {{--<input type="file" name="image" id="image">--}}

                      </div>
                    </div>



                    <div class="col-sm-12 p-t-20">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                          <a href="#definitions" aria-controls="definitions" role="tab" data-toggle="tab">
                            @if ($errors->has('classification_id') || $errors->has('category_id') || $errors->has('group_id') || $errors->has('subgroup_id'))
                              <span class="glyphicon glyphicon-asterisk color-red"></span>
                            @endif
                            {{trans('label.definitions')}}
                          </a>
                        </li>
                        <li role="presentation">
                          <a class="bold" href="#ingredients" aria-controls="ingredients" role="tab" data-toggle="tab">
                            {{trans('label.ingredients')}}
                          </a>
                        </li>
                        <li role="presentation">
                          <a href="#additional" aria-controls="additional" role="tab" data-toggle="tab">
                            @if ($errors->has('additionals.*.name') || $errors->has('additionals.*.value') || $errors->has('additionals.*.default'))
                              <span class="glyphicon glyphicon-asterisk color-red"></span>
                            @endif
                            {{trans('label.additional')}}
                          </a>
                        </li>
                        <li role="presentation">
                          <a href="#others" aria-controls="others" role="tab" data-toggle="tab">
                          @if ($errors->has('preparation') || $errors->has('cost') || $errors->has('moneymaking') || $errors->has('price'))
                            <span class="glyphicon glyphicon-asterisk color-red"></span>
                          @endif
                            {{trans('label.others')}}
                          </a>
                        </li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="definitions">
                          <div class="col-md-12 p-t-20"></div>

                          <div class="form-group{{ $errors->has('classification_id') ? ' has-error' : '' }}">
                              {!! Form::label('classification_id', trans('label.classification_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('classification_id', array_pluck($classifications,'name','id'), $classification_id, ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control'])!!}
                                  @if ($errors->has('classification_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('classification_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                              {!! Form::label('category_id', trans('label.category_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('category_id', array_pluck($categorys,'name','id'), $category_id , ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control','id'=>'slt-category'])!!}
                                  @if ($errors->has('category_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                              {!! Form::label('group_id', trans('label.group_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('group_id', old('category_id') ? array_pluck($groups,'name','id') : array_pluck($groups,'name','id'),$group_id, ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control','id'=>'slt-group'])!!}
                                  @if ($errors->has('group_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('group_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('subgroup_id') ? ' has-error' : '' }}">
                              {!! Form::label('subgroup_id', trans('label.subgroup_id'), ['class' => 'col-md-3 control-label']) !!}
                              <div class="col-md-6">
                                {!!Form::select('subgroup_id', old('group_id') ? array_pluck($subgroups,'name','id') : array_pluck($subgroups,'name','id'), $subgroup_id, ['placeholder' => '--- '.trans('label.select').' ---','class'=>'form-control','id'=>'slt-subgroup'])!!}
                                  @if ($errors->has('subgroup_id'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('subgroup_id') }}</strong>
                                    </span>
                                  @endif
                              </div>
                          </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="ingredients">

                          <div class="col-md-12 p-t-20"></div>

                          <div class="form-group after-add-ingredent">
                            <div class="col-xs-12">
                              <div class="p-l-10">
                                <button class="btn btn-success add-ingredent" type="button"><i class="glyphicon glyphicon-plus"></i> {{trans('action.add')}}</button>
                              </div>
                            </div>
                          </div>

                          @if(old('ingredients') && count(old('ingredients')>0))
                            @foreach (old('ingredients') as $key => $value)
                              @php
                                if($key>$next){
                                 $next=$key;
                                }
                              @endphp


                            <div class="control-group-input" style="margin-top:3px">
                              <div class="row">

                            <div class="col-xs-12">{{--<hr>--}}</div>

                            <div class="col-sm-6 col-md-6">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 <input type="text" name="ingredients[{{$key}}][name]" class="form-control" placeholder="{{trans('label.name')}} da {{trans('label.ingredient')}}" value="{{old("ingredients.$key.name")}}">
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-4 col-md-4">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 {!!Form::select("ingredients[$key][visible]",array_pluck($status_ingredients,'name','id'), null, ['class'=>'form-control'])!!}
                                 {{--<select id="ingredients_{{$key}}_visible" name="ingredients[{{$key}}][visible]" class="form-control"><option value="1">Visible</option><option value="0">Invisible</option></select>--}}
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-2 col-md-2">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('action.remove')}}</button>
                                </div>
                              </div>
                            </div>

                            <div class="clearfix"></div>

                            </div>
                            </div>
                            @endforeach

                        @elseif(count($product->ingredients)>0)

                          @foreach ($product->ingredients as $key => $value)
                            @php
                              if($key>$next){
                               $next=$key;
                              }
                            @endphp


                          <div class="control-group-input" style="margin-top:3px">
                            <div class="row">

                          <div class="col-xs-12">{{--<hr>--}}</div>

                          <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                              <div class="col-sm-12">
                               <input type="text" name="ingredients[{{$key}}][name]" class="form-control" placeholder="{{trans('label.name')}} da {{trans('label.ingredient')}}" value="{{$value->name}}">
                              </div>
                            </div>
                          </div>

                          <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                              <div class="col-sm-12">
                               {!!Form::select("ingredients[$key][visible]",array_pluck($status_ingredients,'name','id'), $value->available, ['class'=>'form-control'])!!}
                               {{--<select id="ingredients_{{$key}}_visible" name="ingredients[{{$key}}][visible]" class="form-control"><option value="1">Visible</option><option value="0">Invisible</option></select>--}}
                              </div>
                            </div>
                          </div>

                          <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                              <div class="col-sm-12">
                               <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('action.remove')}}</button>
                              </div>
                            </div>
                          </div>

                          <div class="clearfix"></div>

                          </div>
                          </div>
                          @endforeach

                        @endif

                        </div>
                        <div role="tabpanel" class="tab-pane" id="additional">

                          <div class="col-md-12 p-t-20"></div>

                            <div class="form-group after-add-additional">
                              <div class="col-xs-12">
                                <div class="p-l-10">
                                  <button class="btn btn-success add-additional" type="button"><i class="glyphicon glyphicon-plus"></i> {{trans('action.add')}}</button>
                                </div>
                              </div>
                            </div>

                            <!-- Init Additional old-->
                            @if(old('additionals') && count(old('additionals')>0))
                              @foreach (old('additionals') as $key => $value)
                                @php
                                  if($key>$next_additional){
                                   $next_additional=$key;
                                  }
                                @endphp


                              <div class="control-group-input" style="margin-top:3px">
                                <div class="row">

                              <div class="col-xs-12">{{--<hr>--}}</div>

                              <div class="col-sm-5 col-md-5">
                                <div class="form-group {{ $errors->has("additionals.$key.name") ? ' has-error' : '' }}">
                                  <label for="additional_{{$key}}_name" class="col-sm-12 c-label">{{trans('label.name')}}</label>
                                  <div class="col-sm-12">
                                   <input type="text" id="additional_{{$key}}_name" name="additionals[{{$key}}][name]" class="form-control" placeholder="" value="{{old("additionals.$key.name")}}" >
                                   @if ($errors->has("additionals.$key.name"))
                                       <span class="help-block">
                                           <strong>{{ $errors->first("additionals.$key.name") }}</strong>
                                       </span>
                                   @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-sm-3 col-md-3">
                                <div class="form-group {{ $errors->has("additionals.$key.value") ? ' has-error' : '' }}">
                                  <label for="additional_{{$key}}_value" class="col-sm-12 c-label">{{trans('label.value')}}</label>
                                  <div class="col-sm-12">
                                   <input type="text" id="additional_{{$key}}_value" name="additionals[{{$key}}][value]" class="form-control" placeholder="" value="{{old("additionals.$key.value")}}">
                                   @if ($errors->has("additionals.$key.value"))
                                       <span class="help-block">
                                           <strong>{{ $errors->first("additionals.$key.value") }}</strong>
                                       </span>
                                   @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-sm-2 col-md-2">
                                <div class="form-group {{ $errors->has("additionals.$key.default") ? ' has-error' : '' }}">
                                  <label for="additional_{{$key}}_default" class="col-sm-12 c-label">{{trans('label.default')}}</label>
                                  <div class="col-sm-12">
                                  <label class="radio-inline">
                                    <input type="radio" name="additionals[{{$key}}][default]" id="additional_{{$key}}_default1" value="1" {{old("additionals.$key.default")==1 ? 'checked' : ''}}> {{trans('label.yes')}}
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="additionals[{{$key}}][default]" id="additional_{{$key}}_default2" value="0" {{old("additionals.$key.default")==0 ? 'checked' : ''}} > {{trans('label.no')}}
                                  </label>
                                  @if ($errors->has("additionals.$key.default"))
                                      <span class="help-block">
                                          <strong>{{ $errors->first("additionals.$key.default") }}</strong>
                                      </span>
                                  @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-sm-2 col-md-2">
                                <div class="form-group">
                                  <div class="col-sm-12">
                                   <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('action.remove')}}</button>
                                  </div>
                                </div>
                              </div>

                              <div class="clearfix"></div>

                              </div>
                              </div>
                              @endforeach
                          @elseif(count($product->additionals)>0)
                            @foreach ($product->additionals as $key => $value)
                              @php
                                if($key>$next_additional){
                                 $next_additional=$key;
                                }
                              @endphp


                            <div class="control-group-input" style="margin-top:3px">
                              <div class="row">

                            <div class="col-xs-12">{{--<hr>--}}</div>

                            <div class="col-sm-5 col-md-5">
                              <div class="form-group {{ $errors->has("additionals.$key.name") ? ' has-error' : '' }}">
                                <label for="additional_{{$key}}_name" class="col-sm-12 c-label">{{trans('label.name')}}</label>
                                <div class="col-sm-12">
                                 <input type="text" id="additional_{{$key}}_name" name="additionals[{{$key}}][name]" class="form-control" placeholder="" value="{{$value->name}}" >
                                 @if ($errors->has("additionals.$key.name"))
                                     <span class="help-block">
                                         <strong>{{ $errors->first("additionals.$key.name") }}</strong>
                                     </span>
                                 @endif
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3 col-md-3">
                              <div class="form-group {{ $errors->has("additionals.$key.value") ? ' has-error' : '' }}">
                                <label for="additional_{{$key}}_value" class="col-sm-12 c-label">{{trans('label.value')}}</label>
                                <div class="col-sm-12">
                                 <input type="text" id="additional_{{$key}}_value" name="additionals[{{$key}}][value]" class="form-control" placeholder="" value="{{$value->price}}">
                                 @if ($errors->has("additionals.$key.value"))
                                     <span class="help-block">
                                         <strong>{{ $errors->first("additionals.$key.value") }}</strong>
                                     </span>
                                 @endif
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-2 col-md-2">
                              <div class="form-group {{ $errors->has("additionals.$key.default") ? ' has-error' : '' }}">
                                <label for="additional_{{$key}}_default" class="col-sm-12 c-label">{{trans('label.default')}}</label>
                                <div class="col-sm-12">
                                <label class="radio-inline">
                                  <input type="radio" name="additionals[{{$key}}][default]" id="additional_{{$key}}_default1" value="1" {{$value->default==1 ? 'checked' : ''}}> {{trans('label.yes')}}
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="additionals[{{$key}}][default]" id="additional_{{$key}}_default2" value="0" {{$value->default==0 ? 'checked' : ''}} > {{trans('label.no')}}
                                </label>
                                @if ($errors->has("additionals.$key.default"))
                                    <span class="help-block">
                                        <strong>{{ $errors->first("additionals.$key.default") }}</strong>
                                    </span>
                                @endif
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-2 col-md-2">
                              <div class="form-group">
                                <div class="col-sm-12">
                                 <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('action.remove')}}</button>
                                </div>
                              </div>
                            </div>

                            <div class="clearfix"></div>

                            </div>
                            </div>
                            @endforeach
                          @endif
                          <!-- End Additional old -->

                        </div>

                        <div role="tabpanel" class="tab-pane" id="others">
                          <div class="col-md-12 p-t-20"></div>

                          <div class="col-sm-offset-8 col-md-4">
                            <div class="form-group{{ $errors->has('preparation') ? ' has-error' : '' }}">
                              {!! Form::label('preparation', trans('label.preparation'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('preparation', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.preparation')]) !!}

                                  @if ($errors->has('preparation'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('preparation') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                              {!! Form::label('cost', trans('label.cost'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('cost', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.cost'), 'id'=>'cost','maxlength'=>'15', 'autocomplete'=>'off']) !!}

                                  @if ($errors->has('cost'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('cost') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group{{ $errors->has('moneymaking') ? ' has-error' : '' }}">
                              {!! Form::label('moneymaking', trans('label.moneymaking'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('moneymaking', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.moneymaking'), 'id'=>'moneymaking','maxlength'=>'6', 'autocomplete'=>'off']) !!}

                                  @if ($errors->has('moneymaking'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('moneymaking') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                              {!! Form::label('price', trans('label.price'), ['class' => 'col-sm-12 c-label']) !!}

                              <div class="col-sm-12">
                                {!! Form::text('price', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.price'), 'id'=>'price','maxlength'=>'15', 'autocomplete'=>'off']) !!}

                                  @if ($errors->has('price'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('price') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>




                    <div class="col-sm-12 p-t-20">
                     <hr>
                     <div class="form-group">
                          <div class="col-md-6">
                              {!! Form::hidden('id', null) !!}
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-red btn-l','type'=>'submit']) !!}
                              {!! link_to_route('product', trans('action.cancel'), $parameters = [], ['class'=>'btn btn-primary btn-l']) !!}
                          </div>
                     </div>
                    </div>
                  {!! Form::close() !!}

                </div>
            </div>

          </div>

        </div>
    </div>
</div>

</div>
@endsection

@section('script')
 <script src="{{ asset('vendors/inputmask/inputmask.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/jquery.inputmask.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.date.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
 <script type="text/javascript" >

 var $templateInput=`<div class="control-group-input" style="margin-top:3px">
<div class="row">
<div class="col-xs-12"></div>

 <div class="col-sm-6 col-md-6">
   <div class="form-group">
     <div class="col-sm-12">
      <input type="text" name="ingredients[:next:][name]" class="form-control" placeholder=":placeholder:">
     </div>
   </div>
 </div>

 <div class="col-sm-4 col-md-4">
   <div class="form-group">
     <div class="col-sm-12">
      <select id="ingredients_:next:_visible" name="ingredients[:next:][visible]" class="form-control"><option value="1">:visible:</option><option value="0">:invisible:</option></select>
     </div>
   </div>
 </div>

 <div class="col-sm-2 col-md-2">
   <div class="form-group">
     <div class="col-sm-12">
      <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> :action_remove:</button>
     </div>
   </div>
 </div>

 <div class="clearfix"></div>
 </div>
 </div>`;

 var $templateInputAdditional=`<div class="control-group-input" style="margin-top:3px">
 <div class="row">
 <div class="col-xs-12"></div>

 <div class="col-sm-5 col-md-5">
   <div class="form-group">
     <label for="additional_:next:_name" class="col-sm-12 c-label">:label_name:</label>
     <div class="col-sm-12">
      <input type="text" id="additional_:next:_name" name="additionals[:next:][name]" class="form-control" placeholder="">
     </div>
   </div>
 </div>

 <div class="col-sm-3 col-md-3">
   <div class="form-group">
     <label for="additional_:next:_value" class="col-sm-12 c-label">:label_value:</label>
     <div class="col-sm-12">
      <input type="text" id="additional_:next:_value" name="additionals[:next:][value]" class="form-control" placeholder="">
     </div>
   </div>
 </div>

 <div class="col-sm-2 col-md-2">
   <div class="form-group">
     <label for="additional_:next:_default" class="col-sm-12 c-label">:label_default:</label>
     <div class="col-sm-12">
     <label class="radio-inline">
       <input type="radio" name="additionals[:next:][default]" id="additional_:next:_default1" value="1"> :label_yes:
     </label>
     <label class="radio-inline">
       <input type="radio" name="additionals[:next:][default]" id="additional_:next:_default2" value="0" checked> :label_no:
     </label>
     </div>
   </div>
 </div>

 <div class="col-sm-2 col-md-2">
   <div class="form-group">
     <div class="col-sm-12">
      <button class="btn btn-danger remove-ingredent" type="button"><i class="glyphicon glyphicon-remove"></i> :action_remove:</button>
     </div>
   </div>
 </div>

 <div class="clearfix"></div>
 </div>
 </div>`;

 $action_remove="{{trans('action.remove')}}";
 $label_invisible="{{trans('label.invisible')}}";
 $label_visible="{{trans('label.visible')}}";
 $placeholder_ingredient="{{trans('label.name')}}"+' de '+"{{trans('label.ingredient')}}";
 $label_yes= "{{trans('label.yes')}}";
 $label_no = "{{trans('label.no')}}";
 $label_value = "{{trans('label.value')}}";
 $label_name = "{{trans('label.name')}}";
 $label_default = "{{trans('label.default')}}";

 $("#slt-category").prop('selectedIndex');

 $(document).ready(function(){
   var next = {{$next}}+1;
  $("#preparation").inputmask("99:99:99");
  var next_additional = {{$next_additional}}+1;
 $("#preparation").inputmask("99:99:99");
 $(".additional_price").inputmask("999999999.99");
 $("#cost").inputmask({
   alias: 'decimal',
   groupSeparator: '',
   autoGroup: true,
   digits: 2,
   radixPoint: ',',
   placeholder: '0',
   digitsOptional: false
 });
 $("#moneymaking").inputmask({
   alias: 'decimal',
   groupSeparator: '',
   autoGroup: true,
   digits: 2,
   radixPoint: ',',
   placeholder: '0',
   digitsOptional: false
 });
 $("#price").inputmask({
   alias: 'decimal',
   groupSeparator: '',
   autoGroup: true,
   digits: 2,
   radixPoint: ',',
   placeholder: '0',
   digitsOptional: false
 });

  $(".add-ingredent").click(function(){
      var html = $templateInput
      .replace(/:next:/g, next)
      .replace(':action_remove:',$action_remove)
      .replace(':invisible:',$label_invisible)
      .replace(':visible:',$label_visible)
      .replace(':placeholder:',$placeholder_ingredient);
      next = next + 1;
      $(".after-add-ingredent").after(html);
  });

  $("body").on("click",".remove-ingredent",function(){
      if (!confirm("Tem certeza de que deseja excluir?")) {
          return false;
      }
      $(this).parents(".control-group-input").fadeOut()
      $(this).parents(".control-group-input").remove();
  });

  $(".add-additional").click(function(){
      var html = $templateInputAdditional
      .replace(/:next:/g, next_additional)
      .replace(':action_remove:',$action_remove)
      .replace(':label_name:',$label_name)
      .replace(':label_default:',$label_default)
      .replace(':label_value:',$label_value)
      .replace(':label_yes:',$label_yes)
      .replace(':label_no:',$label_no);
      next_additional = next_additional + 1;
      $(".after-add-additional").after(html);
  });

});
$("#slt-category").change(function(){

         var group = $("#slt-group");
         var subgroup = $("#slt-subgroup");

         // Guardamos el select de alumnos
         var category = $(this);
         //console.log(state.val());
         var url = base_url+'/dashboard/category/group';
         $token=$('meta[name="csrf-token"]').attr('content');
         console.log($token);
         if($(this).val() != '')
         {
             $.ajax({
                 data: { id : category.val()},
                 url:  url,
                 type:  'POST',
                 dataType: 'json',
                 beforeSend: function ()
                 {
                     category.prop('disabled', true);
                     group.prop('disabled', true);
                     subgroup.find('option').remove();
                     subgroup.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
                 },
                 success:  function (r)
                 {
                     category.prop('disabled', false);

                     // Limpiamos el select
                     group.find('option').remove();
                     group.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
                     $(r.groups).each(function(i, v){ // indice, valor
                         group.append('<option value="' + v.id + '">' + v.name + '</option>');
                     })

                     group.prop('disabled', false);
                 },
                 error: function(r)
                 {
                     //console.log(r);
                     //alert('Ocurrio un error en el servidor ..');
                     group.find('option').remove();
                     group.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
                     category.prop('disabled', false);
                 }
             });
         }else
         {
             group.find('option').remove();
             group.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
             //group.prop('disabled', true);
             subgroup.find('option').remove();
             subgroup.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');

             //subgroup.prop('disabled', true);
         }
     });
  $("#slt-group").change(function(){

           var subgroup = $("#slt-subgroup");

           // Guardamos el select de alumnos
           var group = $(this);
           //console.log(state.val());
           var url = base_url+'/dashboard/group/subgroup';
           $token=$('meta[name="csrf-token"]').attr('content');
           console.log($token);
           if($(this).val() != '')
           {
               $.ajax({
                   data: { id : group.val()},
                   url:  url,
                   type:  'POST',
                   dataType: 'json',
                   beforeSend: function ()
                   {
                       group.prop('disabled', true);
                       subgroup.prop('disabled', true);
                   },
                   success:  function (r)
                   {
                       group.prop('disabled', false);

                       // Limpiamos el select
                       subgroup.find('option').remove();
                       subgroup.append('<option value="">' + '--- '+'{{trans('label.select')}}'+' ---' + '</option>');
                       $(r.subgroups).each(function(i, v){ // indice, valor
                           subgroup.append('<option value="' + v.id + '">' + v.name + '</option>');
                       })

                       subgroup.prop('disabled', false);
                   },
                   error: function(r)
                   {
                       //console.log(r);
                       //alert('Ocurrio un error en el servidor ..');
                       subgroup.find('option').remove();
                       group.prop('disabled', false);
                   }
               });
           }else
           {
               subgroup.find('option').remove();
               subgroup.prop('disabled', true);
           }
       })

       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
      }
      $("#image").change(function(){
          readURL(this);
      });

      function calcularCost(input){
        var $cost = $("#cost").val().replace(',','.');
        var $porce = $("#moneymaking").val().replace(',','.');
        var $price = $("#price").val().replace(',','.');
        if(!isNaN($cost)) {
          if($cost > 0){
            if(!isNaN($porce)){
              if($porce > 0){
                $price=((eval($cost)*eval($porce))/100)+eval($cost);
                $price=round($price,2);
                $price=$price.toString().replace('.',',');
                $("#price").val($price);
              }else{
                $price='';
                $("#price").val($price);
                /*if(!isNaN($price)){
                  if($price > 0){
                    $porce=((eval($cost)-eval($price))/eval($cost))*100;
                    $porce=$porce*-1;
                    $("#moneymaking").val($porce);
                  }
                }*/
              }
            }else{
              $price='';
              $("#price").val($price);
            }
          }else{
            $price='';
            $("#price").val($price);
          }
        }else{
          $price='';
          $("#price").val($price);
        }
      }


      function calcularMoneymaking(input){
        var $cost = $("#cost").val().replace(',','.');
        var $porce = $("#moneymaking").val().replace(',','.');
        var $price = $("#price").val().replace(',','.');
        if(!isNaN($cost)) {
          if($cost > 0){
            if(!isNaN($porce)){
              if($porce > 0){
                $price=((eval($cost)*eval($porce))/100)+eval($cost);
                $price=round($price,2);
                $price=$price.toString().replace('.',',');
                $("#price").val($price);
              }else{
                $price='';
                $("#price").val($price);
              }
            }else{
              $price='';
              $("#price").val($price);
            }
          }else{
            $price='';
            $("#price").val($price);
          }
        }else{
          $price='';
          $("#price").val($price);
        }
      }

      function calcularPrice(input){
        var $cost = $("#cost").val().replace(',','.');
        var $porce = $("#moneymaking").val().replace(',','.');
        var $price = $("#price").val().replace(',','.');
        if(!isNaN($cost)) {
          if($cost > 0){
            if(!isNaN($price)){
              if($price > 0){
                $porce=((eval($cost)-eval($price))/eval($cost))*100;
                $porce=$porce*-1;
                $porce=round($porce,2);
                $porce=$porce.toString().replace('.',',');
                $("#moneymaking").val($porce);
              }else{
                $price='';
                $("#moneymaking").val($price);
              }
            }else{
              $price='';
              $("#moneymaking").val($price);
            }
          }else{
            $price='';
            $("#moneymaking").val($price);
          }
        }else{
          $price='';
          $("#moneymaking").val($price);
        }
      }

      $("#cost").keyup(function(){
          calcularCost(this);
      });

      $("#moneymaking").keyup(function(){
          calcularMoneymaking(this);
      });

      $("#price").keyup(function(){
          calcularPrice(this);
      });

      function round(value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
      }
 </script>
@endsection
