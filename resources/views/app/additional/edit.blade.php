@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li><a href="{{route('additional')}}">{{trans('module.additional')}}</a></li>
        <li class="active">{{trans('action.edit')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.update')!!} {{trans('action.edit')}}</div>

                <div class="panel-body">

                  {!! Form::model($additional,['route' => ['additional.update',$additional->id],'class'=>'form-horizontal']) !!}
                    {{ method_field('PUT') }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                          {!! Form::text('name', null,['tabindex'=>'1','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.name').' da '.trans('module.additional')]) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                      {!! Form::label('price', trans('label.value'), ['class' => 'col-md-4 control-label']) !!}

                      <div class="col-md-6">
                        {!! Form::text('price', null,['tabindex'=>'2','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.value'),'id'=>'price','maxlength'=>'15', 'autocomplete'=>'off']) !!}

                          @if ($errors->has('value'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('value') }}</strong>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="form-group {{ $errors->has("default") ? ' has-error' : '' }}">

                        {!! Form::label('default', trans('label.default'), ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                        <label class="radio-inline">
                          {!! Form::radio('default', '1', $additional->available==1 ? true : false,['tabindex'=>'3']) !!} {{trans('label.yes')}}
                          {{--<input type="radio" name="default" id="defaultyes" value="1" {{old("default")==1 ? 'checked' : ''}}> {{trans('label.yes')}}--}}
                        </label>
                        <label class="radio-inline">
                          {!! Form::radio('default', '0', $additional->available==0 ? true : false,['tabindex'=>'3']) !!} {{trans('label.no')}}
                          {{---<input type="radio" name="default" id="defaultno" value="0" {{old("default")==0 ? 'checked' : ''}} > {{trans('label.no')}}--}}
                        </label>
                        @if ($errors->has("default"))
                            <span class="help-block">
                                <strong>{{ $errors->first("default") }}</strong>
                            </span>
                        @endif
                        </div>
                    </div>


                    <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              {!! Form::hidden('id', null) !!}
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-red btn-l','type'=>'submit']) !!}
                              {!! link_to_route('additional', trans('action.cancel'), $parameters = [], ['class'=>'btn btn-primary btn-l']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
 <script src="{{ asset('vendors/inputmask/inputmask.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/jquery.inputmask.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.date.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('vendors/inputmask/inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
 <script src="{{ asset('js/jquery.ya-enter2tab.js') }}" type="text/javascript"></script>

 <script type="text/javascript" >

 $("form").enableEnterToTab({ captureTabKey: true });

 $(document).ready(function(){
   //$("#price").inputmask('currency');
  $("#price").inputmask({
    alias: 'decimal',
    groupSeparator: '',
    autoGroup: true,
    digits: 2,
    radixPoint: ',',
    placeholder: '0',
    digitsOptional: false
  });
 });
 </script>

@endsection
