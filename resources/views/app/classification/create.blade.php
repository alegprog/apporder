@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li><a href="{{route('classification')}}">{{trans('module.classification')}}</a></li>
        <li class="active">{{trans('action.new')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.store')!!} {{trans('action.new')}}</div>

                <div class="panel-body">

                  {!! Form::open(['route' => 'classification.store','class'=>'form-horizontal']) !!}
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                          {!! Form::text('name', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.name').' da '.trans('module.classification')]) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              {!! Form::button(trans('action.save'),['class'=>'btn btn-red btn-l','type'=>'submit']) !!}
                              {!! link_to_route('classification', trans('action.cancel'), $parameters = [], ['class'=>'btn btn-primary btn-l']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
