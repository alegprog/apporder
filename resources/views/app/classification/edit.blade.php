@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li><a href="{{route('classification')}}">{{trans('module.classification')}}</a></li>
        <li class="active">{{trans('action.edit')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>{{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.update')!!} {{trans('action.edit')}}</div>

                <div class="panel-body">

                  {!! Form::model($classification,['route' => ['classification.update', $classification->id],'class'=>'form-horizontal']) !!}
                    {{ method_field('PUT') }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                          {!! Form::text('name', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.name').' da '.trans('module.classification')]) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              {!! Form::hidden('id', null) !!}
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-red btn-l','type'=>'submit']) !!}
                              {!! link_to_route('classification', trans('action.cancel'), $parameters = [], ['class'=>'btn btn-primary btn-l']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}

                  <hr>
                    <a href="javascript:void(0);" class="btn btn-primary btn-l pull-right" role="button" data-toggle="modal" data-target="#myModal" >{!!trans('icon.new')!!} {{trans('action.add')}} <br> {{trans('label.size')}}</a>

                    <div class="clearfix"></div>

                    <table class="table table-striped" id="tableItemSize">
                      <thead>
                        <tr>
                          <th>{{trans('label.name')}}</th>
                          <th>{{trans('label.abbreviated')}}</th>
                          <th>{{trans('label.quantity')}}</th>
                          <th>{{trans('label.action')}}</th>
                        </tr>
                      <thead>
                      <tbody id="tableItemSizeBody">
                        @foreach ($classification->subclassifications as $subclassification)
                        <tr>
                          <td>{{$subclassification->name}}</td>
                          <td>{{$subclassification->abbreviated}}</td>
                          <td>{{$subclassification->quantity}}</td>
                          <td>
                            {!! Form::open(['route' => ['subclassification.delete',$subclassification->id],'class'=>'','name'=>'frm-'.$subclassification,'id'=>'frm-'.$subclassification->id ]) !!}
                              {!! method_field('delete') !!}
                              <button type="button" onclick="deleteSize(this)" class="btn btn-red btn-delete pull-right">
                                {!!trans('icon.delete')!!} {{trans('action.delete')}}
                              </button>
                              {!! Form::hidden('id',$subclassification->id) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>

                </div>
            </div>


        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{!!trans('icon.new')!!} {{trans('action.add')}} {{trans('label.size')}}</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['route' => 'subclassification.store','class'=>'form-horizontal','id'=>'frmSize']) !!}
          {{ csrf_field() }}

          <div class="input-name form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

              <div class="col-md-7">
                {!! Form::text('name', null,['tabindex'=>'1','class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.name')]) !!}


                      <span class="span-error-name help-block hidden">
                          <strong class="merror-name"></strong>
                      </span>

              </div>

          </div>

          <div class="input-abbreviated form-group{{ $errors->has('abbreviated') ? ' has-error' : '' }}">
              {!! Form::label('abbreviated', trans('label.abbreviated'), ['class' => 'col-md-4 control-label']) !!}

              <div class="col-md-7">
                {!! Form::text('abbreviated', null,['tabindex'=>'2','class' => 'form-control', 'placeholder'=>trans('label.name').' '.trans('label.abbreviated')]) !!}


                      <span class="span-error-abbreviated help-block hidden">
                          <strong class="merror-abbreviated"></strong>
                      </span>

              </div>

          </div>

          <div class="input-quantity form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
              {!! Form::label('quantity', trans('label.quantity'), ['class' => 'col-md-4 control-label']) !!}

              <div class="col-md-7">
                {!! Form::text('quantity', null,['tabindex'=>'3','class' => 'form-control', 'placeholder'=>trans('label.quantity').' de '.trans('label.division')]) !!}


                      <span class="span-error-quantity help-block hidden">
                          <strong class="merror-quantity" ></strong>
                      </span>

              </div>

          </div>

          <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    {!! Form::hidden('id', $classification->id) !!}
                    {!! Form::button(trans('action.save'),['class'=>'btn btn-red btn-l','type'=>'button','id'=>'btn_add_size']) !!}
                </div>
            </div>
          <div class="form-group">
            <div class="col-md-12">

              <div id="alert-message-success" class="alert alert-success fade in hidden">
                <i class="glyphicon glyphicon-ok"></i> <strong> {{trans('message.success')}}</strong> <span id="message-success" ></span>
              </div>
              <div id="alert-message-danger" class="alert alert-danger fade in hidden">
                <i class="glyphicon glyphicon-remove"></i> <strong> {{trans('message.danger')}}</strong> <span id="message-danger" ></span>
              </div>

            </div>
          </div>
        {!! Form::close() !!}
      </div>
      {{--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>--}}
    </div>
  </div>
</div>
@endsection

@section('script')
 <script src="{{ asset('js/jquery.ya-enter2tab.js') }}" type="text/javascript"></script>
 <script type="text/javascript" >

 $("#frmSize").enableEnterToTab({ captureTabKey: true });

 var $campo = ["name", "abbreviated", "quantity"];

 $('#myModal').on('show.bs.modal', function (event) {
   var $frm = 'frmSize';
   clearMessage($frm,$campo);
 })

var template = `<tr>
         <td>:name:</td>
         <td>:abbreviated:</td>
         <td>:quantity:</td>
         <td>
           <form action=':url:'>
           :token:
           :input:
           :button:
           :code:
           <form>
         </td>
       </tr>`;

  $('#btn_add_size').click(function () {
      var $itemSize = $('#tableItemSize').find('tbody');
      var $frm = 'frmSize';
      clearMessage($frm,$campo);
      $('#btn_add_size').prop('disabled',true);
      $.ajax({
          url: $('#frmSize').attr("action"),
          method: $('#frmSize').attr("method"),
          data: $('#frmSize').serialize(),
          dataType: 'json',
          success: function(res)
          {
            setTimeout(function(){
              res.data.forEach(function (data) {
                $frm = res.frm;
                var item = template
                .replace(':name:', data.name)
                .replace(':abbreviated:', data.abbreviated)
                .replace(':quantity:', data.quantity)
                .replace(':url:',$frm.url)
                .replace(':token:',$frm.token)
                .replace(':input:',$frm.input)
                .replace(':button:',$frm.button)
                .replace(':code:',$frm.code);

                $itemSize.append($(item).fadeIn(1500));

              });
              $('#frm').trigger("reset");

              $message=res.success;
              $('#message-success').text($message);
              $('#alert-message-success').removeClass('hidden');
              $('#frmSize').trigger("reset");
              $('#btn_add_size').prop('disabled',false);
            }, 1000);
            //console.log(res);

          },
          error: function(jqXHR, textStatus, errorThrown)
          {
            status = jqXHR.status;
            errors = jqXHR.responseJSON;

            setTimeout(function(){

              if(status==500){
                console.log('Error 500');
              }else if (status==403) {
                console.log('Error 403');
              }else if (status==404) {
                console.log('Error 404');
              }else if (status==422) {
                console.log('Error 422');
                validate(errors,$frm);
              }else if (status==401) {
                console.log('Error 401');
              }else{
                console.log('Error desconocido');
              }

              $('#btn_add_size').prop('disabled',false);
              //$('#alert-message-danger').removeClass('hidden');
            }, 1000);

          }
      });
  });

  function deleteSize(el){
          //e.preventDefault();
          if (!confirm("Tem certeza de que deseja excluir?")) {
              return false;
          }

          var row     = $(el).parents('tr');
          var form    = $(el).parents('form');
          var url     = form.attr('action');

          $('#alert').show();
          $.post(url, form.serialize(), function(result){
              row.fadeOut();
              $('#alert').html(result.message);
          }).fail(function(){
              $('#alert').html("algo salió mal");
          });

  }


  function clearMessage(frm,campo){
      $('#alert-message-success').addClass('hidden');
      $('#alert-message-danger').addClass('hidden');
      //var campo = ["name", "abbreviated", "quantity"];
      fLen = campo.length;
      for (i = 0; i < fLen; i++) {
        $('#'+frm).find(".span-error-"+campo[i]+"").addClass('hidden');
        $('#'+frm).find(".input-"+campo[i]+"").removeClass('has-error');
        $('#'+frm).find(".merror-"+campo[i]+"").html('');
      }
  }

    function validate(merror,frm){
        //var campo = ["name", "abbreviated", "quantity"];
        var elemento="";
        var contador=0;
        $.each(merror, function (n, c) {
        if(contador==0){
        elemento=n;
        }
        contador++;

         $.each(this, function (name, value) {
            var error=value;
            //$("#error-"+n+"_mensaje").html(error);
            $('#'+frm).find(".input-"+n+"").addClass('has-error');
            $('#'+frm).find(".merror-"+n+"").html(error);
            $('#'+frm).find(".span-error-"+n+"").removeClass('hidden');
            //$('#frmSize strong').find(".error-"+n+"").text(error);
            //$('#frmSize span '+".help-block .error-"+n+""));
            //$('#frmSize span').find(".help-block .error-"+n+"").removeClass('hidden');
         });
      });
    }
  </script>
@endsection
