@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}">{!! trans('icon.dashboard') !!} Dashboard</a></li>
        <li class="active">{{trans('module.ingredient')}}</li>
      </ol>
      @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <i class="glyphicon glyphicon-ok"></i> <strong> {{trans('message.success')}}</strong> {{ session('success') }}
            </div>
      @endif
      <div class="col-md-12 p-b-10">
        <div class="row">
          {!! Html::decode(link_to_route('ingredient.create', trans('icon.new').' '.trans('action.new'), $parameters = [], ['class'=>'btn btn-primary btn-l pull-right'])) !!}
        </div>
      </div>
        <div class="col-md-12">
          <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">{!!trans('icon.list')!!} {{trans('action.list')}}</div>

                <div class="panel-body">

                    <table class="table table-striped">
                      <thead>
                       <tr>
                        <th>
                          {{ trans('label.name') }}
                        </th>
                        <th>
                          {{ trans('label.status') }}
                        </th>
                        <th class="width-100" >
                          {{ trans('label.action') }}
                        </th>
                       </tr>
                      </thead>
                      <tbody>
                        @foreach ($ingredients as $ingredient)
                        <tr>
                          <td>{{$ingredient->name}}</td>
                          <td>{{$ingredient->status}}</td>
                          <td>
                            {!! Html::decode(link_to_route('ingredient.edit', trans('icon.edit').' '.trans('action.edit'), ['id'=>$ingredient->id],['class'=>'btn btn-primary'])) !!}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>


                </div>


            </div>
          </div>
        </div>
    </div>
</div>
@endsection
