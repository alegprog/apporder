@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{!! trans('icon.dashboard') !!} Dashboard</div>

                <div class="panel-body">

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.classification')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('classification')}}" class="btn btn-success btn-l btn-block" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('classification.create')}}" class="btn btn-primary btn-l btn-block" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.category')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('category')}}" class="btn btn-success btn-block btn-l" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('category.create')}}" class="btn btn-primary btn-block btn-l" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.group')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('group')}}" class="btn btn-success btn-block btn-l" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('group.create')}}" class="btn btn-primary btn-block btn-l" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.subgroup')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('subgroup')}}" class="btn btn-success btn-block btn-l" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('subgroup.create')}}" class="btn btn-primary btn-block btn-l" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.ingredient')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('ingredient')}}" class="btn btn-success btn-block btn-l" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('ingredient.create')}}" class="btn btn-primary btn-block btn-l" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.additional')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('additional')}}" class="btn btn-success btn-block btn-l" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('additional.create')}}" class="btn btn-primary btn-block btn-l" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        {{trans('module.product')}}
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-6">
                            <a href="{{route('product')}}" class="btn btn-success btn-block btn-l" role="button">{!!trans('icon.list')!!} {{trans('action.list')}}</a>
                          </div>
                          <div class="col-xs-6">
                            <a href="{{route('product.create')}}" class="btn btn-primary btn-block btn-l" role="button">{!!trans('icon.new')!!} {{trans('action.new')}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
