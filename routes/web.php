<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $groups=\App\Entities\Group::get();
    return view('welcome',compact('groups'));
    //return redirect('dashboard');
})->name('menu');

Route::post('logout', 'Auth\LoginController@logout')
->name('logout');

//Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

require __DIR__ .'/web/main.php';
