<?php

Route::get('/dashboard/group',
   [
     'as' =>'group',
     'uses' =>'GroupController@index'
   ]
);

Route::get('/dashboard/group/create',
   [
     'as' =>'group.create',
     'uses' =>'GroupController@create'
   ]
 );

Route::get('/dashboard/group/edit/{id}',
   [
     'as' =>'group.edit',
     'uses' =>'GroupController@edit'
   ]
);

Route::post('/dashboard/group/store',
   [
     'as' =>'group.store',
     'uses' =>'GroupController@store'
   ]
 );

Route::put('/dashboard/group/update/{id}',
   [
     'as' =>'group.update',
     'uses' =>'GroupController@update'
   ]
 );

 Route::post('/dashboard/group/subgroup',
  [
    'as' => 'group.subgroup',
    'uses' =>'GroupController@sltGroup'
  ]
);
