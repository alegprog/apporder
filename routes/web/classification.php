<?php

Route::get('/dashboard/classification',
   [
     'as' =>'classification',
     'uses' =>'ClassificationController@index'
   ]
);

Route::get('/dashboard/classification/create',
   [
     'as' =>'classification.create',
     'uses' =>'ClassificationController@create'
   ]
 );

Route::get('/dashboard/classification/edit/{id}',
   [
     'as' =>'classification.edit',
     'uses' =>'ClassificationController@edit'
   ]
);

Route::post('/dashboard/classification/store',
   [
     'as' =>'classification.store',
     'uses' =>'ClassificationController@store'
   ]
 );

Route::put('/dashboard/classification/update/{id}',
   [
     'as' =>'classification.update',
     'uses' =>'ClassificationController@update'
   ]
 );
