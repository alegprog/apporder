<?php

Route::get('/dashboard/product',
   [
     'as' =>'product',
     'uses' =>'ProductController@index'
   ]
);

Route::get('/dashboard/product/create',
   [
     'as' =>'product.create',
     'uses' =>'ProductController@create'
   ]
 );

Route::get('/dashboard/product/edit/{id}',
   [
     'as' =>'product.edit',
     'uses' =>'ProductController@edit'
   ]
);

Route::post('/dashboard/product/store',
   [
     'as' =>'product.store',
     'uses' =>'ProductController@store'
   ]
 );

Route::put('/dashboard/product/update/{id}',
   [
     'as' =>'product.update',
     'uses' =>'ProductController@update'
   ]
 );

 Route::delete('/dashboard/product/delete/{id}',
    [
      'as' =>'product.delete',
      'uses' =>'ProductController@destroy'
    ]
  );
