<?php

Route::get('login', 'SocialAuthController@login')
->name('auth');

Route::get('/auth/facebook', 'SocialAuthController@facebook')
->name('auth.facebook');
Route::get('/auth/facebook/callback', 'SocialAuthController@callbackFacebook')
->name('facebook.callback');
Route::post('/auth/facebook/register', 'SocialAuthController@registerFacebook')
->name('facebook.register');

Route::get('/auth/google', 'SocialAuthController@google')
->name('auth.google');
Route::get('/auth/google/callback', 'SocialAuthController@callbackGoogle')
->name('google.callback');
Route::post('/auth/google/register', 'SocialAuthController@registerGoogle')
->name('google.register');
