<?php

Route::get('/order/{group}', 'OrderController@listgroup')->name('order.group');
Route::get('/order/{subgroup}/product', 'OrderController@listproduct')->name('order.product');
Route::get('/order/{product}/product/show', 'OrderController@productShow')->name('order.product.show');
