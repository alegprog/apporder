<?php

Route::get('/dashboard/subclassification',
   [
     'as' =>'subclassification',
     'uses' =>'SubclassificationController@index'
   ]
);

Route::get('/dashboard/subclassification/create',
   [
     'as' =>'subclassification.create',
     'uses' =>'SubclassificationController@create'
   ]
 );

Route::get('/dashboard/subclassification/edit/{id}',
   [
     'as' =>'subclassification.edit',
     'uses' =>'SubclassificationController@edit'
   ]
);

Route::post('/dashboard/subclassification/store',
   [
     'as' =>'subclassification.store',
     'uses' =>'SubclassificationController@store'
   ]
 );

Route::put('/dashboard/subclassification/update/{id}',
   [
     'as' =>'subclassification.update',
     'uses' =>'SubclassificationController@update'
   ]
 );

 Route::delete('/dashboard/subclassification/delete/{id}',
    [
      'as' =>'subclassification.delete',
      'uses' =>'SubclassificationController@destroy'
    ]
  );
