<?php

Route::get('/dashboard/category',
   [
     'as' =>'category',
     'uses' =>'CategoryController@index'
   ]
);

Route::get('/dashboard/category/create',
   [
     'as' =>'category.create',
     'uses' =>'CategoryController@create'
   ]
 );

Route::get('/dashboard/category/edit/{id}',
   [
     'as' =>'category.edit',
     'uses' =>'CategoryController@edit'
   ]
);

Route::post('/dashboard/category/store',
   [
     'as' =>'category.store',
     'uses' =>'CategoryController@store'
   ]
 );

Route::put('/dashboard/category/update/{id}',
   [
     'as' =>'category.update',
     'uses' =>'CategoryController@update'
   ]
 );

 Route::post('/dashboard/category/group',
  [
    'as' => 'category.group',
    'uses' =>'CategoryController@sltCategory'
  ]
);
