<?php

Route::get('/dashboard/ingredient',
   [
     'as' =>'ingredient',
     'uses' =>'IngredientController@index'
   ]
);

Route::get('/dashboard/ingredient/create',
   [
     'as' =>'ingredient.create',
     'uses' =>'IngredientController@create'
   ]
 );

Route::get('/dashboard/ingredient/edit/{id}',
   [
     'as' =>'ingredient.edit',
     'uses' =>'IngredientController@edit'
   ]
);

Route::post('/dashboard/ingredient/store',
   [
     'as' =>'ingredient.store',
     'uses' =>'IngredientController@store'
   ]
 );

Route::put('/dashboard/ingredient/update/{id}',
   [
     'as' =>'ingredient.update',
     'uses' =>'IngredientController@update'
   ]
 );
