<?php

Route::get('/dashboard/additional',
   [
     'as' =>'additional',
     'uses' =>'AdditionalController@index'
   ]
);

Route::get('/dashboard/additional/create',
   [
     'as' =>'additional.create',
     'uses' =>'AdditionalController@create'
   ]
 );

Route::get('/dashboard/additional/edit/{id}',
   [
     'as' =>'additional.edit',
     'uses' =>'AdditionalController@edit'
   ]
);

Route::post('/dashboard/additional/store',
   [
     'as' =>'additional.store',
     'uses' =>'AdditionalController@store'
   ]
 );

Route::put('/dashboard/additional/update/{id}',
   [
     'as' =>'additional.update',
     'uses' =>'AdditionalController@update'
   ]
 );
