<?php

Route::get('/dashboard/subgroup',
   [
     'as' =>'subgroup',
     'uses' =>'SubgroupController@index'
   ]
);

Route::get('/dashboard/subgroup/create',
   [
     'as' =>'subgroup.create',
     'uses' =>'SubgroupController@create'
   ]
 );

Route::get('/dashboard/subgroup/edit/{id}',
   [
     'as' =>'subgroup.edit',
     'uses' =>'SubgroupController@edit'
   ]
);

Route::post('/dashboard/subgroup/store',
   [
     'as' =>'subgroup.store',
     'uses' =>'SubgroupController@store'
   ]
 );

Route::put('/dashboard/subgroup/update/{id}',
   [
     'as' =>'subgroup.update',
     'uses' =>'SubgroupController@update'
   ]
 );
