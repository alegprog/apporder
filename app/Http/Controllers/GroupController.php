<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Group\StoreGroupRequest;
use App\Http\Requests\App\Group\UpdateGroupRequest;
use App\Entities\Group;
use App\Entities\Category;

class GroupController extends Controller
{
    public function index(){
      $groups=Group::all();
      return view('app.group.index',compact('groups'));
    }

    public function create(){
      $categories=Category::all();
      return view('app.group.create',compact('categories'));
    }

    public function store(StoreGroupRequest $request){
      $group = new Group();
      $group->name=$request->name;
      $group->category_id=$request->category_id;
      $group->save();

      return redirect()->route('group')->with('success',trans('message.store'));
    }

    public function edit(Request $request, $id){
      $group=Group::findOrFail($id);
      $categories=Category::all();
      return view('app.group.edit',compact('group','categories'));
    }

    public function update(UpdateGroupRequest $request, $id){

      $group_id=$request->id;
      $group = Group::findOrFail($group_id);
      $group->name=$request->name;
      $group->category_id=$request->category_id;
      $group->save();

      return redirect()->route('group')->with('success',trans('message.update'));
    }

    public function sltGroup(Request $request)
    {
      $group=Group::with(['subgroups' => function ($query){
      }])->findOrFail($request->id);

      return ['subgroups'=>$group->subgroups];
    }
}
