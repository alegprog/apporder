<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Subclassification\StoreSubclassificationRequest;
use App\Entities\Subclassification;
use Form;

class SubclassificationController extends Controller
{
    public function store(StoreSubclassificationRequest $request){

      //dd($request->all());
      $subclassification = new Subclassification();
      $subclassification->name = $request->name;
      $subclassification->abbreviated = $request->abbreviated;
      $subclassification->quantity = $request->quantity;
      $subclassification->classification_id = $request->id;

      /*
      {!! Form::open(['route' => ['subclassification.delete',$subclassification->id],'class'=>'','name'=>'frm-'.$subclassification,'id'=>'frm-'.$subclassification->id ]) !!}
        {!! method_field('delete') !!}
        <button type="button" class="btn btn-danger btn-delete pull-right">
          {!!trans('icon.delete')!!} {{trans('action.delete')}}
        </button>
        {!! Form::hidden('id',$subclassification->id) !!}
      {!! Form::close() !!}
      */

      $token=$request->_token;
      if($subclassification->save()){
        return response()->json([
          'success' => trans('message.store'),
          'data'=>[$subclassification],
          'frm'=>[
            'url'=>route('subclassification.delete',['id'=>$subclassification->id]),
            'token'=>'<input type="hidden" name="_token" value="'.$token.'">',
            'input'=>'<input type="hidden" name="_method" value="delete">',
            'code'=>'<input type="hidden" name="id" value="'.$subclassification->id.'">',
            'button'=>'<button type="button" onclick="deleteSize(this)" class="btn btn-red btn-delete pull-right">'.trans('icon.delete').' '.trans('action.delete').'</button>'
          ],
          'status'=>'OK'
        ]);
      }else{
        return response([
          'status'=>'Error'
        ], 500);
      }
    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $subclassification = Subclassification::findOrFail($id);
            if($subclassification->delete()){
              return response()->json([
                'success' => trans('message.delete'),
                'status'=>'OK'
              ]);
            }else{
              return response([
                'status'=>'Error'
              ], 500);
            }
        }
    }
}
