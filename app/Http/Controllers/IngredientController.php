<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Ingredient\StoreIngredientRequest;
use App\Http\Requests\App\Ingredient\UpdateIngredientRequest;
use App\Entities\Ingredient;

class IngredientController extends Controller
{
    public function index(){
      $ingredients=Ingredient::all();

      return view('app.ingredient.index',compact('ingredients'));
    }

    public function create(){
      $status=[
        ['id'=>'1', 'name'=>trans('label.visible')],
        ['id'=>'0', 'name'=>trans('label.invisible')],
      ];
      return view('app.ingredient.create',compact('ingredients','status'));
    }

    public function store(StoreIngredientRequest $request){
      $ingredient = new Ingredient();
      $ingredient->name=$request->name;
      $ingredient->available=$request->status;
      $ingredient->save();

      return redirect()->route('ingredient')->with('success',trans('message.store'));
    }

    public function edit(Request $request, $id){
      $ingredient=Ingredient::findOrFail($id);
      $status=[
        ['id'=>'1', 'name'=>trans('label.visible')],
        ['id'=>'0', 'name'=>trans('label.invisible')],
      ];
      return view('app.ingredient.edit',compact('ingredient','status'));
    }

    public function update(UpdateIngredientRequest $request, $id){

      $ingredient=$request->id;
      $ingredient=Ingredient::findOrFail($id);
      $ingredient->name=$request->name;
      $ingredient->available=$request->status;
      $ingredient->save();

      return redirect()->route('ingredient')->with('success',trans('message.update'));
    }


}
