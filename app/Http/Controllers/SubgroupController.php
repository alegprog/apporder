<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Subgroup\StoreSubgroupRequest;
use App\Http\Requests\App\Subgroup\UpdateSubgroupRequest;
use App\Entities\Subgroup;
use App\Entities\Group;

class SubgroupController extends Controller
{
    public function index(){
      $subgroups=Subgroup::with(['group' => function ($query) {
      }])->get();
      return view('app.subgroup.index',compact('subgroups'));
    }

    public function create(){
      $groups=Group::all();
      return view('app.subgroup.create',compact('groups'));
    }

    public function store(StoreSubgroupRequest $request){
      $subgroup = new Subgroup();
      $subgroup->name=$request->name;
      $subgroup->group_id=$request->group_id;
      $subgroup->save();

      return redirect()->route('subgroup')->with('success',trans('message.store'));
    }

    public function edit(Request $request, $id){
      $groups=Group::all();
      $subgroup=Subgroup::findOrFail($id);
      return view('app.subgroup.edit',compact('subgroup','groups'));
    }

    public function update(UpdateSubgroupRequest $request, $id){

      $subgroup_id=$request->id;
      $subgroup = Subgroup::findOrFail($subgroup_id);
      $subgroup->name=$request->name;
      $subgroup->group_id=$request->group_id;
      $subgroup->save();

      return redirect()->route('subgroup')->with('success',trans('message.update'));
    }
}
