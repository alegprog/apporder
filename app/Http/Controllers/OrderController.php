<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Group;
use App\Entities\Subgroup;

class OrderController extends Controller
{
    public function listgroup($group){
      $groups=Group::all();

      $selectgroup=Group::with(['subgroups' => function ($query) {
      }])->findOrFail($group);

      //$subgroups=$group->subgroups;

      //dd($subgroups);

      return view('app.order.listgroup',compact('groups','selectgroup'));
    }

    public function listproduct($subgroup){
      $groups=Group::all();

      $selectsubgroup=Subgroup::with(['products' => function ($query) {
        $query->with(['ingredients' => function ($query) {
          $query->where('available',1);
        }]);
      }])->with(['group' => function ($query) {
      }])->findOrFail($subgroup);

      //dd($selectsubgroup);

      return view('app.order.listproduct',compact('groups','selectsubgroup'));
    }
}
