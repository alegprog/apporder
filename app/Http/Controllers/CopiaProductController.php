<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Product\StoreProductRequest;
use App\Http\Requests\App\Product\UpdateProductRequest;
use App\Entities\Product;
use App\Entities\Classification;
use App\Entities\Category;
use App\Entities\Group;
use App\Entities\Subgroup;
use App\Entities\Ingredient;
use App\Entities\Additional;
use Storage;

class ProductController extends Controller
{

  
   public function index(){
    $products=Product::with(['classification' => function ($query) {
    }])->with(['category' => function ($query) {
    }])->with(['group' => function ($query) {
    }])->with(['subgroup' => function ($query) {
    }])->get();
    return view('app.product.index',compact('products'));
   }

   public function create(){
     $classifications=Classification::all();
     $categorys=Category::all();
     $groups=Group::all();
     $availables=[
        ['id'=>'1', 'name'=>trans('label.yes')],
        ['id'=>'0', 'name'=>trans('label.no')],
      ];
      $status_ingredients=[
        ['id'=>'1', 'name'=>trans('label.visible')],
        ['id'=>'0', 'name'=>trans('label.invisible')],
      ];
     return view('app.product.create',compact('classifications','categorys','groups','availables','status_ingredients'));
   }

   public function store(StoreProductRequest $request){
     //dd($request->all());
     $product = new Product();
     $product->code = $request->code;
     $product->description = $request->description;
     $product->barcode = $request->barcode;
     $product->available = $request->available;
     $product->order = $request->order;
     $product->classification_id = $request->classification_id;
     $product->category_id = $request->category_id;
     $product->group_id = $request->group_id;
     $product->subgroup_id = $request->subgroup_id;
     $product->preparation = $request->preparation;
     $product->cost = $request->cost;
     $product->moneymaking = $request->moneymaking;
     $product->price = $request->price;
     $product->image = '';
     if($product->save()){

       if ($request->hasFile('image')){
         $path = $request->file('image')->store('product'.'/'.$product->id,'public');
         $product->image=$path;
         $product->save();
       }else{

       }

       $ingredients=[];
       $collectIngredient=collect($request->ingredients);
       $unique = $collectIngredient->unique('name');
       $uniqueIngredients=$unique->values()->all();

       $additionals=[];
       $collectAdditional=collect($request->additionals);
       $unique = $collectAdditional->unique('name');
       $uniqueAdditionals=$unique->values()->all();

       foreach($uniqueIngredients as $item)
       {
           if(empty($item['name'])){
             continue;
           }

           $ingredients[]= new Ingredient([
             'name'=> $item['name'],
             'available'=> $item['visible']
           ]);
        }

        if(count($ingredients)>0){
          $product->ingredients()->saveMany($ingredients);
        }

        foreach($uniqueAdditionals as $item)
        {
            if(empty($item['name'])){
              continue;
            }

            $additionals[]= new Additional([
              'name'=>$item['name'],
              'price' => $item['value'],
              'default'=> $item['default'],
            ]);
         }

         if(count($additionals)>0){
           $product->additionals()->saveMany($additionals);
         }

     }



     return redirect()->route('product')->with('success',trans('message.store'));
   }

   public function edit(Request $request, $id){
     $product=Product::findOrFail($id);
     $classifications=Classification::all();
     $categorys=Category::all();
     $groups=Group::where('category_id',$product->category_id)->get();
     $subgroups=Subgroup::where('group_id',$product->group_id)->get();
     $availables=[
        ['id'=>'1', 'name'=>trans('label.yes')],
        ['id'=>'0', 'name'=>trans('label.no')],
      ];
      $status_ingredients=[
        ['id'=>'1', 'name'=>trans('label.visible')],
        ['id'=>'0', 'name'=>trans('label.invisible')],
      ];
     return view('app.product.edit',compact('product','classifications','categorys','groups','subgroups','availables','status_ingredients'));
   }

   public function update(UpdateProductRequest $request, $id){

     $product_id=$request->id;
     $product = Product::findOrFail($product_id);
     $product->code = $request->code;
     $product->description = $request->description;
     $product->barcode = $request->barcode;
     $product->available = $request->available;
     $product->order = $request->order;
     $product->classification_id = $request->classification_id;
     $product->category_id = $request->category_id;
     $product->group_id = $request->group_id;
     $product->subgroup_id = $request->subgroup_id;
     $product->preparation = $request->preparation;
     $product->cost = $request->cost;
     $product->moneymaking = $request->moneymaking;
     $product->price = $request->price;
     //$product->save();

     if($product->save()){

       if ($request->hasFile('image')){
          $image =$product->image;
          if(Storage::disk('public')->has($image)){
            Storage::disk('public')->delete($image);
          }
          $path = $request->file('image')->store('product'.'/'.$product->id,'public');
          $product->image=$path;
          $product->save();
        }else{
          //$company->logo='';
        }
        $collectIngredient=collect($request->ingredients);
        $unique = $collectIngredient->unique('name');
        $uniqueIngredients=$unique->values()->all();
        $product->ingredients()->delete();

        $collectAdditional=collect($request->additionals);
        $unique = $collectAdditional->unique('name');
        $uniqueAdditionals=$unique->values()->all();
        $product->additionals()->delete();

        $ingredients=[];
        $additionals=[];

        foreach($uniqueIngredients as $item)
        {
            if(empty($item['name'])){
              continue;
            }

            $ingredients[]= new Ingredient([
              'name'=>$item['name'],
              'available'=> $item['visible']
            ]);
         }

         if(count($ingredients)>0){
           $product->ingredients()->saveMany($ingredients);
         }

         foreach($uniqueAdditionals as $item)
         {
             if(empty($item['name'])){
               continue;
             }

             $additionals[]= new Additional([
               'name'=>$item['name'],
               'price' => $item['value'],
               'default'=> $item['default'],
             ]);
          }

          if(count($additionals)>0){
            $product->additionals()->saveMany($additionals);
          }



    }

     return redirect()->route('product')->with('success',trans('message.update'));
   }

   public function destroy(Request $request, $id)
   {
       $product= Product::findOrFail($id);
       $product->delete();
       return redirect()->route('product')->with('success',trans('message.delete'));
   }
}
