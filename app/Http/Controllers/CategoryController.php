<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Category\StoreCategoryRequest;
use App\Http\Requests\App\Category\UpdateCategoryRequest;
use App\Entities\Category;

class CategoryController extends Controller
{
    public function index(){
      $categorys=Category::all();
      return view('app.category.index',compact('categorys'));
    }

    public function create(){
      return view('app.category.create');
    }

    public function store(StoreCategoryRequest $request){
      $category = new Category();
      $category->name=$request->name;
      $category->save();

      return redirect()->route('category')->with('success',trans('message.store'));
    }

    public function edit(Request $request, $id){
      $category=Category::findOrFail($id);
      return view('app.category.edit',compact('category'));
    }

    public function update(UpdateCategoryRequest $request, $id){

      $category_id=$request->id;
      $category = Category::findOrFail($category_id);
      $category->name=$request->name;
      $category->save();

      return redirect()->route('category')->with('success',trans('message.update'));
    }

    public function sltCategory(Request $request)
    {
      $category=Category::with(['groups' => function ($query){
      }])->findOrFail($request->id);

      return ['groups'=>$category->groups];
    }
}
