<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Addicional\StoreAddicionalRequest;
use App\Http\Requests\App\Addicional\UpdateAddicionalRequest;
use App\Entities\Additional;

class AdditionalController extends Controller
{
  public function index(){
    $additionals=Additional::all();

    return view('app.additional.index',compact('additionals'));
  }

  public function create(){
    $status=[
      ['id'=>'1', 'name'=>trans('label.yes')],
      ['id'=>'0', 'name'=>trans('label.no')],
    ];
    return view('app.additional.create',compact('status'));
  }

  public function store(StoreAddicionalRequest $request){
    //dd($request->all());
    $additional = new Additional();
    $additional->name=$request->name;
    $additional->price=$request->value;
    $additional->available=$request->default;
    $additional->save();

    return redirect()->route('additional')->with('success',trans('message.store'));
  }

  public function edit(Request $request, $id){
    $additional=Additional::findOrFail($id);
    $status=[
      ['id'=>'1', 'name'=>trans('label.yes')],
      ['id'=>'0', 'name'=>trans('label.no')],
    ];
    return view('app.additional.edit',compact('additional','status'));
  }

  public function update(UpdateAddicionalRequest $request, $id){

    $additional=$request->id;
    $additional=Additional::findOrFail($id);
    $additional->name=$request->name;
    $additional->price=$request->value;
    $additional->available=$request->default;
    $additional->save();

    return redirect()->route('additional')->with('success',trans('message.update'));
  }
}
