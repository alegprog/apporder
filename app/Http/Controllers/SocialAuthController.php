<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Entities\SocialProfile;
use Socialite;

class SocialAuthController extends Controller
{

  public function __construct()
  {
      $this->middleware('guest');
  }

  public function login()
  {
    return view('app.auth.login');
  }

  public function facebook()
  {
    return Socialite::driver('facebook')->redirect();
  }

  public function google()
  {
    return Socialite::driver('google')->redirect();
  }

  public function callbackFacebook()
  {
    $user = Socialite::driver('facebook')->user();

    $existing = User::whereHas('socialProfiles', function ($query) use ($user) {
      $query->where('social_id', $user->id)->where('provider', 'facebook');
    })->first();

    if ($existing !== null) {
      auth()->login($existing);
      return redirect('/');
    }

    session()->flash('facebookUser', $user);

    return view('app.user.facebook', [
      'user' => $user,
    ]);
  }

  public function registerFacebook(Request $request)
  {
    $data = session('facebookUser');
    //$avatar=$data->avatar ? '' ;
    //dd($data,$data->avatar);

    $user = User::whereEmail($data->email)->first();

    if (!$user) {

      $user = User::create([
        'name' => $data->name,
        'email' => $data->email,
        'avatar' => $data->avatar,
        'password' => str_random(16),
        'type'=>'client',
      ]);

    }

    $profile = SocialProfile::create([
      'provider'=> 'facebook',
      'social_id' => $data->id,
      'user_id' => $user->id,
    ]);

    auth()->login($user);
    return redirect('/');

 }

 public function callbackGoogle()
 {
   $user = Socialite::driver('google')->user();

   $existing = User::whereHas('socialProfiles', function ($query) use ($user) {
     $query->where('social_id', $user->id)->where('provider', 'google');
   })->first();

   if ($existing !== null) {
     auth()->login($existing);
     return redirect('/');
   }

   session()->flash('googleUser', $user);

   return view('app.user.google', [
     'user' => $user,
   ]);
 }

 public function registerGoogle(Request $request)
 {
   $data = session('googleUser');
   //$avatar=$data->avatar ? '' ;
   //dd($data,$data->avatar);

   $user = User::whereEmail($data->email)->first();

   if (!$user) {

     $user = User::create([
       'name' => $data->name,
       'email' => $data->email,
       'avatar' => $data->avatar,
       'password' => str_random(16),
       'type'=>'client',
     ]);

   }

   $profile = SocialProfile::create([
     'provider'=> 'google',
     'social_id' => $data->id,
     'user_id' => $user->id,
   ]);

   auth()->login($user);
   return redirect('/');

}



}
