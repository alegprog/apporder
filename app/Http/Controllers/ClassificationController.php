<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\App\Classification\StoreClassificationRequest;
use App\Http\Requests\App\Classification\UpdateClassificationRequest;
use App\Entities\Classification;

class ClassificationController extends Controller
{
    public function index(){
      $classifications=Classification::all();
      return view('app.classification.index',compact('classifications'));
    }

    public function create(){
      return view('app.classification.create');
    }

    public function store(StoreClassificationRequest $request){
      $classification = new Classification();
      $classification->name=$request->name;
      $classification->save();

      return redirect()->route('classification')->with('success',trans('message.store'));
    }

    public function edit(Request $request, $id){
      $classification=Classification::with(['subclassifications' => function ($query) {
      }])->findOrFail($id);
      return view('app.classification.edit',compact('classification'));
    }

    public function update(UpdateClassificationRequest $request, $id){

      $classification_id=$request->id;
      $classification = Classification::findOrFail($classification_id);
      $classification->name=$request->name;
      $classification->save();

      return redirect()->route('classification')->with('success',trans('message.update'));
    }
}
