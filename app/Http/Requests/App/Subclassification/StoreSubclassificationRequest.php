<?php

namespace App\Http\Requests\App\Subclassification;

use Illuminate\Foundation\Http\FormRequest;

class StoreSubclassificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'name'=>'required|unique:subclassifications,name,NULL,id,classification_id,'.$this->get('id').'',
          'abbreviated'=>'required|unique:subclassifications,abbreviated,NULL,id,classification_id,'.$this->get('id').'',
          'quantity'=>'required|integer',
      ];
    }
}
