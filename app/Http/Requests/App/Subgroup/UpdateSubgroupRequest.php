<?php

namespace App\Http\Requests\App\Subgroup;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubgroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'group_id'=>'required',
          'name'=>'required|unique:subgroups,name,'.$this->get('id').',id,group_id,'.$this->get('group_id').'',
        ];
    }
}
