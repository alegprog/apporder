<?php

namespace App\Http\Requests\App\Addicional;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddicionalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all()
    {
        $input = parent::all();
        $price=empty($this->input('price')) ? '0,00' : $this->input('price');
        $input['value'] = str_replace(',','.',$price);

        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'name'=> 'required|string|unique:additionals,name',
          'value'=> 'required|numeric|min:0',
          'default'=> 'required',
      ];
    }
}
