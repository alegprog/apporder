<?php

namespace App\Http\Requests\App\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all()
    {
        $input = parent::all();

        $input['cost'] = str_replace(',','.',$this->input('cost'));
        $input['moneymaking'] = str_replace(',','.',$this->input('moneymaking'));
        $input['price'] = str_replace(',','.',$this->input('price'));

        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          //'code'=>'required|unique:products,code,'.$this->get('id').',id',
          'description'=>'required|unique:products,description,'.$this->get('id').',id',
          'barcode'=>'required|unique:products,barcode,'.$this->get('id').',id',
          'available'=>'required',
          'order'=>'required|integer',
          'classification_id'=>'required',
          'category_id'=>'required',
          'group_id'=>'required',
          'subgroup_id'=>'required',
          'preparation'=>'required',
          'cost'=>'required|numeric|min:0.01',
          'moneymaking'=>'required|numeric|min:0.01',
          'price'=>'required|numeric|min:0.01',
          'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          'additionals.*.name'=> 'required|string',
          //'additionals.*.value'=> 'required|numeric',
          //'additionals.*.default'=> 'required',
      ];
    }
}
