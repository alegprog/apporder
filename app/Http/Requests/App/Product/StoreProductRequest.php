<?php

namespace App\Http\Requests\App\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        //dd($this);
        //dd($this->request);
        //dd($this->input('addmore'));
        return true;

        /*$cost= str_replace(',','.',$this->input('cost'));
        $moneymaking= str_replace(',','.',$this->input('moneymaking'));
        $price= str_replace(',','.',$this->input('price'));
        $this->request->add(['cost'=>$cost,'moneymaking'=>$moneymaking,'price'=>$price]);*/
    }

    public function all()
    {
        $input = parent::all();

        $input['cost'] = str_replace(',','.',$this->input('cost'));
        $input['moneymaking'] = str_replace(',','.',$this->input('moneymaking'));
        $input['price'] = str_replace(',','.',$this->input('price'));

        return $input;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        //$this->input('cost')='aa';
        //Request::create('dashboard/product/store','POST',['a'=>'a']);


        //dd($this->request);


        return [
            //'code'=>'required|unique:products,code',
            'description'=>'required|unique:products,description',
            'barcode'=>'required|unique:products,barcode',
            'available'=>'required',
            'order'=>'required|integer',
            'classification_id'=>'required',
            'category_id'=>'required',
            'group_id'=>'required',
            'subgroup_id'=>'required',
            'preparation'=>'required',
            'cost'=>'required|numeric|min:0.01',
            'moneymaking'=>'required|numeric|min:0.01',
            'price'=>'required|numeric|min:0.01',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'additionals.*.name'=> 'required|string',
            //'additionals.*.value'=> 'required|numeric',
            //'additionals.*.default'=> 'required',
        ];
    }
}
