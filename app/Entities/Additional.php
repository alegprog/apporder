<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Additional extends Model
{
    protected $fillable = [
        'name', 'price', 'available',
    ];

    public function getStatusAttribute(){
      if($this->available==1){
        return trans('label.yes');
      }else{
        return trans('label.no');
      }
    }

    public function products()
    {
        return $this->belongsToMany('App\Entities\Product')->withTimestamps();
    }



    /*public function product()
    {
        return $this->belongsTo('App\Entities\Product');
    }*/
}
