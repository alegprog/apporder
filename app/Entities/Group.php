<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function subgroups()
    {
        return $this->hasMany('App\Entities\Subgroup');
    }

    public function category()
    {
        return $this->belongsTo('App\Entities\Category');
    }
}
