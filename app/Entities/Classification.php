<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
  public function subclassifications()
  {
      return $this->hasMany('App\Entities\Subclassification');
  }
}
