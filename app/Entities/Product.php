<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
  use SoftDeletes;

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['deleted_at'];

  public function classification()
  {
      return $this->belongsTo('App\Entities\Classification');
  }

  public function category()
  {
      return $this->belongsTo('App\Entities\Category');
  }

  public function group()
  {
      return $this->belongsTo('App\Entities\Group');
  }

  public function subgroup()
  {
      return $this->belongsTo('App\Entities\Subgroup');
  }

  public function getAvailabilityAttribute()
  {
        $value = $this->available ? trans('label.yes') : trans('label.no') ;
        return $value;
  }

  public function ingredients()
  {
      return $this->belongsToMany('App\Entities\Ingredient')->withTimestamps();
  }

  public function additionals()
  {
      return $this->belongsToMany('App\Entities\Additional')->withTimestamps();
  }
}
