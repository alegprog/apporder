<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Subclassification extends Model
{
  public function classification()
  {
      return $this->belongsTo('App\Entities\Classification');
  }
}
