<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Subgroup extends Model
{
    public function group()
    {
        return $this->belongsTo('App\Entities\Group');
    }

    public function products()
    {
        return $this->hasMany('App\Entities\Product');
    }
}
