<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{

  protected $fillable = [
      'name', 'available',
  ];


  /*public function product()
  {
      return $this->belongsTo('App\Entities\Product');
  }*/

  public function getStatusAttribute(){
    if($this->available==1){
      return trans('label.visible');
    }else{
      return trans('label.invisible');
    }
  }

  public function products()
  {
      return $this->belongsToMany('App\Entities\Product')->withTimestamps();
  }

}
