<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  public function groups()
  {
      return $this->hasMany('App\Entities\Group');
  }
}
